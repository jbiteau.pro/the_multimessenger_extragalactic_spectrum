import os
import matplotlib.pyplot as plt
plt.style.use('./allcrs.mplstyle')
import numpy as np
from astropy import units as u
from astropy import constants as const
from astropy.table import QTable
import copy 
from cycler import cycler

def MySaveFig(fig, pltname, pngsave=True):
	print ('Saving plot as ' + pltname + '.pdf')
	fig.savefig(pltname + '.pdf', bbox_inches='tight', dpi=300)
	if pngsave:
		fig.savefig(pltname + '.png', bbox_inches='tight', dpi=300)

class TheSpectrum():
   
	def __init__(self, main_axes, sec_axes=None, ms=8,lw=2, label_type='observatory', show_gray_errors=False, is_write_caption_data=False):
		self.main_axes = main_axes
		self.unit_nu = main_axes['x_axis']['unit']
		self.unit_nuInu = main_axes['y_axis']['unit']				
		self.sec_axes = sec_axes
		self.ms = ms
		self.lw = lw	
		self.label_type = label_type
		self.is_write_caption_data = is_write_caption_data
		self.ref_id_M = []
		self.ref_id_IGL = []
		self.ref_id_UL = []
		self.show_gray_errors = show_gray_errors
		self.list_is_lower_legend = []
		self.list_posx = []
		self.list_labels = []
		self.list_colors = []
		self.dataset = []
		print ('Calling TheSpectrum constructor')
	
	def FigSetup(self, Shape='Wide', credits=False):
		if Shape=='Wide':
			fig = plt.figure(figsize=(16.5,10.5))
			fudge_factor = 9.4
		elif Shape=='Rectangular':
			fig = plt.figure(figsize=(10.0,8))			
			fudge_factor = 6.8
		elif Shape=='Between':
			fig = plt.figure(figsize=(14.,10.5))
			fudge_factor = 9.6
						
		ax = fig.add_subplot(111)
		if credits: 
			normy = np.power(self.main_axes['y_axis']['val'][1]/self.main_axes['y_axis']['val'][0],1/fudge_factor)
			ax.text(self.main_axes['x_axis']['val'][1], self.main_axes['y_axis']['val'][0]/normy, "Credits: J. Biteau '23", fontsize=10, color='tab:gray', ha='right')		
		self.SetAxes(ax)
		return fig,ax
		
	def SetAxes(self, ax):
		ax.minorticks_off()
		#Set x-axis
		ax.set_xscale('log')
		ax.set_xlim(self.main_axes['x_axis']['val'])
		ax.set_xlabel(self.main_axes['x_axis']['title'])
		#Set y-axis
		ax.set_yscale('log')
		ax.set_ylim(self.main_axes['y_axis']['val'])
		ax.set_ylabel(self.main_axes['y_axis']['title'])
		#Set twin axes
		if self.sec_axes!=None:
			if self.sec_axes['x_axis']!=None:
				#Set x-axis	
				ax2 = ax.twiny()
				ax2.minorticks_off()
				ax2.set_xscale('log')
				ax2.set_xlabel(self.sec_axes['x_axis']['title'], labelpad=20)
				lim = np.array(self.main_axes['x_axis']['val'])*self.unit_nu
				lim = lim.to(self.sec_axes['x_axis']['unit'], equivalencies=u.spectral()).value 
				ax2.set_xlim(lim)		
			if self.sec_axes['y_axis']!=None:
				#Set y-axis	
				ax3 = ax.twinx()
				ax3.minorticks_off()			
				ax3.set_yscale('log')
				ax3.set_ylabel(self.sec_axes['y_axis']['title'], labelpad=20)
				lim = np.array(self.main_axes['y_axis']['val'])*self.unit_nuInu
				lim = lim.to(self.sec_axes['y_axis']['unit'], equivalencies=u.spectral()).value 
				ax3.set_ylim(lim)
			
	def ResetColorCycle(self, ax, col='#1f77b4'):
		colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
		i = colors.index(col)
		new_colors = colors[i:]+colors[:i]
		ax.set_prop_cycle(cycler(color=new_colors))

	def SetColorCycle(self, ax, col):
		ax.set_prop_cycle(cycler(color=[col]))
		
	def load_data(self, ax, lower_legend, posx, folder, files):
		labels, colors = [], []
		self.show_data(ax, labels, colors, folder, files)
		self.list_is_lower_legend.append(lower_legend)				
		self.list_posx.append(posx)
		self.list_labels.append(labels)
		self.list_colors.append(colors)
		
		return colors[0]
		
	def show_data(self, ax, labels, colors, folder, files):
		for f in files:
			filename = os.path.join('data',folder,f+'.ecsv')
			data = QTable.read(filename, format='ascii.ecsv')
			color, label = self.plot_data(ax, data)
			if color!=None:
				labels.append(label)
				colors.append(color)
			
	def plot_data(self, ax, data, sig_sel=1, color=None, alpha=1, keep_dataset=True):
		nu = data.columns[0]
		x = copy.copy(nu.to(self.unit_nu,equivalencies=u.spectral()).value)
		y = copy.copy(data.columns[1])
		y_errn = copy.copy(data.columns[2])
		y_errp = copy.copy(data.columns[3])

		if data.colnames[1]=='T':#surprisingly u.thermodynamic_temperature does not appear to work with np.arrays !?!
			ytmp, ytmp_errn, ytmp_errp = [], [], []
			unit_tmp = u.MJy / u.sr
			for j in range(len(y)):
				equiv_T = u.thermodynamic_temperature(nu[j])
				ytmp.append(y[j].to(unit_tmp,equivalencies=equiv_T).value)
				ytmp_errn.append(y_errn[j].to(unit_tmp,equivalencies=equiv_T).value)
				ytmp_errp.append(y_errp[j].to(unit_tmp,equivalencies=equiv_T).value)
			y = np.array(ytmp)*unit_tmp
			y_errn = np.array(ytmp_errn)*unit_tmp
			y_errp = np.array(ytmp_errp)*unit_tmp
		elif data.colnames[1]=='dNdE':
			y*=nu**2
			y_errn*=nu**2
			y_errp*=nu**2
				
		equiv_nuInu = u.spectral_density(nu)
		y = y.to(self.unit_nuInu,equivalencies=equiv_nuInu).value
		y_errn = y_errn.to(self.unit_nuInu,equivalencies=equiv_nuInu).value
		y_errp = y_errp.to(self.unit_nuInu,equivalencies=equiv_nuInu).value

		zeros = np.zeros_like(x)
		sel = (y_errn>0)*(y>=sig_sel*y_errn)
		if np.sum(sel):#at least one point above significant threshold
			if data.meta['observable_type']=='M':
				if color==None: p = ax.plot(x[sel], y[sel], marker='o', ms = self.ms,fillstyle='none', linewidth=0, zorder=2, alpha=alpha)
				else: p = ax.plot(x[sel], y[sel], marker='o', ms = self.ms,fillstyle='none', linewidth=0, zorder=2, alpha=alpha, color=color)
				ax.errorbar(x[sel], y[sel], yerr=[y_errn[sel],y_errp[sel]], marker='', ms = self.ms, elinewidth=self.lw, linewidth=0, color = p[0].get_color(), zorder=1, alpha=alpha)
				self.ref_id_M.append(data.meta['reference_id'])
				data_points = [x[sel], y[sel], y_errn[sel], y_errp[sel]]
			elif data.meta['observable_type']=='H':
				if color==None: p = ax.plot(x[sel], y[sel], marker='s', ms = self.ms,fillstyle='none', linewidth=0, zorder=2, alpha=alpha)
				else: p = ax.plot(x[sel], y[sel], marker='s', ms = self.ms,fillstyle='none', linewidth=0, zorder=2, alpha=alpha, color=color)
				ax.errorbar(x[sel], y[sel], yerr=[y_errn[sel],y_errp[sel]], marker='', ms = self.ms, elinewidth=self.lw, linewidth=0, color = p[0].get_color(), zorder=1, alpha=alpha)
				self.ref_id_M.append(data.meta['reference_id'])
				data_points = [x[sel].astype(float), y[sel].astype(float), y_errn[sel].astype(float), y_errp[sel].astype(float)]
			elif data.meta['observable_type']=='IGL':
				if color==None: p = ax.plot(x[sel], y[sel], marker='^', ms = self.ms,fillstyle='none', linewidth=0, zorder=2, alpha=alpha)
				else: p = ax.plot(x[sel], y[sel], marker='^', ms = self.ms,fillstyle='none', linewidth=0, zorder=2, alpha=alpha, color=color)
				if self.show_gray_errors: ax.errorbar(x[sel], y[sel], yerr=[zeros[sel], y_errp[sel]], color='gray', marker='', ms = self.ms, elinewidth=self.lw-1, linewidth=0, zorder=0, alpha=alpha)
				ax.errorbar(x[sel], y[sel], yerr=[y_errn[sel], zeros[sel]], marker='', ms = self.ms, elinewidth=self.lw, linewidth=0, color = p[0].get_color(), zorder=1, alpha=alpha)
				self.ref_id_IGL.append(data.meta['reference_id'])
				data_points = [x[sel], y[sel], y_errn[sel], np.infty*np.ones_like(y_errp[sel])]
			elif data.meta['observable_type']=='UL':
				if color==None: p = ax.plot(x[sel], y[sel], marker='v', ms = self.ms,fillstyle='none', linewidth=0, zorder=2)
				else: p = ax.plot(x[sel], y[sel], marker='v', ms = self.ms,fillstyle='none', linewidth=0, zorder=2, color=color)
				if self.show_gray_errors: ax.errorbar(x[sel], y[sel], yerr=[y_errn[sel], zeros[sel]], color='gray', marker='', ms = self.ms, elinewidth=self.lw-1, linewidth=0, zorder=0, alpha=alpha)
				ax.errorbar(x[sel], y[sel], yerr=[zeros[sel], y_errp[sel]], marker='', ms = self.ms, elinewidth=self.lw, linewidth=0, color = p[0].get_color(), zorder=0, alpha=alpha)
				self.ref_id_UL.append(data.meta['reference_id'])
				data_points = [x[sel], y[sel], np.infty*np.ones_like(y_errn[sel]), y_errp[sel]]
			else:
				print('WARNING: observable type not found in', data.meta['label'])
			if keep_dataset: self.dataset.append(np.array(data_points).T.astype(float))
			return p[0].get_color(), data.meta[self.label_type]
		else: return None, None

	
	def plot_line(self, ax, nu, nuInu, color=None, ls='-', lw = 2, label=None):
		x = nu.to(self.unit_nu,equivalencies=u.spectral()).value
		y = nuInu.to(self.unit_nuInu,equivalencies=u.spectral_density(nu)).value
		ax.plot(x,y, color=color, ls=ls, label=label, lw=lw, zorder=-1)

	def plot_range(self, ax, nu, nuInu0, nuInu1, hatch=None, color=None, edgecolor=None, alpha=1, lw = 0, label=None):
		x = nu.to(self.unit_nu,equivalencies=u.spectral()).value
		y0 = nuInu0.to(self.unit_nuInu,equivalencies=u.spectral_density(nu)).value
		y1 = nuInu1.to(self.unit_nuInu,equivalencies=u.spectral_density(nu)).value		
		ax.fill_between(x,y0,y1, color=color, edgecolor=edgecolor, alpha=alpha, label=label, lw=lw, hatch=hatch)
		
	def plot_model(self, ax, func, color=None, ls='-', lw = 2, label=None):
		x = np.logspace(np.log10(self.main_axes['x_axis']['val'][0]),np.log10(self.main_axes['x_axis']['val'][1]))
		nu = x*self.unit_nu
		nuInu = func(nu)
		y = nuInu.to(self.unit_nuInu,equivalencies=u.spectral_density(nu)).value
		ax.plot(x,y, color=color, ls=ls, label=label, lw=lw, zorder=-1)
		
	def model_all(self, ax, label='label'):
		files = ['khaire2019']
		colors = ['k','g']
		linestyles = ['-','--']
		for i, f in enumerate(files):
			filename = os.path.join('model',f+'.ecsv')
			data = QTable.read(filename, format='ascii.ecsv')
			nu, nuInu = data.columns[0], data.columns[1]
			self.plot_line(ax, nu, nuInu, color=colors[i], ls=linestyles[i], label=data.meta[label])	
			
	def model_cxb(self, ax, label='label'):
		filename = os.path.join('model/CXB','faucher-giguere2020.ecsv')
		data = QTable.read(filename, format='ascii.ecsv')
		nu, nuInu = data.columns[0], data.columns[1]
		self.plot_line(ax, nu, nuInu, ls='--', label=data.meta[label])	
	
		def cxb_model(nu):
			C=10.15e-2*1/(u.keV*u.cm**2*u.s*u.sr)
			gamma1=1.32
			gamma2=2.88
			nu_B=(29.99*u.keV).to(nu.unit,equivalencies=u.spectral())
			return C*nu**2/((nu/nu_B)**gamma1+(nu/nu_B)**gamma2)
		func = lambda nu: cxb_model(nu)
		self.plot_model(ax, func, 'tab:red', ls='-.', label="Best-fit (Ajello+ '08)")	


	def model_cgb(self, ax):
		def cgb_model(nu):
			I100=1.48e-7/(u.MeV*u.cm**2*u.s*u.sr)
			gamma=2.31
			nu_cut=362*u.GeV
			nu_100 = 100.*u.MeV
			return I100*nu**2*(nu/nu_100)**(-gamma)*np.exp(-nu/nu_cut)
		func = lambda nu: cgb_model(nu)
		self.plot_model(ax, func, 'tab:red', ls='-.', label="Best-fit (Ackermann+ '15)")	   
    
	def model_zl(self, ax, label='label', nuInu_550nm=13.2*u.nW/(u.sr*u.m**2)):#minimum of 418 nW m-2 sr-1, 13,200 at 30deg solar elongation (2020P&SS..19004973L)
		folder = 'model/COB'
		files = ['solar_spectrum']
		colors = ['gray']
		linestyles = ['-']
		for i, f in enumerate(files):
			filename = os.path.join(folder,f+'.ecsv')
			data = QTable.read(filename, format='ascii.ecsv')
			l, nuInu = data.columns[0], data.columns[1]
			l = l.to(u.um,equivalencies=u.spectral())
			nuInu_interp = np.interp(0.55*u.um, l, nuInu)
			nuInu = nuInu_550nm*(nuInu/nuInu_interp)			
			nuInu_frac = np.round((100.*nuInu_550nm/(1.32E4*u.nW/(u.sr*u.m**2))).value,1)
			self.plot_line(ax, l, nuInu, color=colors[i], ls=linestyles[i], lw=1, label=f'{nuInu_frac}\% of Zodi at 30'+r"$^{\circ}$ solar elongation in ecliptic plane")			
			
	def model_cob(self, ax, label='label', imodels=0):
		folder = 'model/COB'
		files = [	['gilmore2012', 'andrews18', 'saldana2021', 'andrews_agn_18'],#possibly best of each type
					['koushan2021', 'saldana2021', 'finke2022'],#recent
					['stecker2016','franceschini2017', 'andrews18', 'lagos2019'],#post gamma rays
					['franceschini2008', 'finke2010', 'dominguez2011', 'gilmore2012']#pre gamma rays
				]
		linestyles = ['-.','-','--',':',(0, (3, 1, 1, 1))]
		for i, f in enumerate(files[imodels]):
			filename = os.path.join(folder,f+'.ecsv')
			data = QTable.read(filename, format='ascii.ecsv')
			nu, nuInu = data.columns[0], data.columns[1]

			self.plot_line(ax, nu, nuInu, ls=linestyles[i], lw=2, label=data.meta[label])
			
	def nuInu_CMB(self, nu):
		T_CMB = 2.7255*u.K
		kT = const.k_B*T_CMB
		hnu = const.h*nu
		Inu = 2*hnu*(nu/const.c)**2/(np.exp(hnu/kT)-1)
		return (nu*Inu/u.sr).to(u.nW/(u.m**2*u.sr))

	def model_range_cmb(self, ax):						
		nu=np.logspace(7,12)*u.Hz
		nuInu_min = self.main_axes['y_axis']['val'][0]*self.unit_nuInu
		self.plot_range(ax, nu, nuInu_min*np.ones_like(nu.value), self.nuInu_CMB(nu), color='gray', alpha=0.1)

	def model_cmb(self, ax):						
		nu=np.logspace(8.82,11.9)*u.Hz
		self.plot_line(ax, nu, self.nuInu_CMB(nu), color='gray', lw=2)						
			
	def model_crb(self, ax, label='label'):
		folder = 'model/CRB'
		files = ['tompkins2023']
		linestyles = [':','--','-.',(0, (3, 1, 1, 1))]
		for i, f in enumerate(files):
			filename = os.path.join(folder,f+'.ecsv')
			data = QTable.read(filename, format='ascii.ecsv')
			nu, nuInu = data.columns[0], data.columns[1]
			self.plot_line(ax, nu, nuInu, ls=linestyles[i], lw=2, label=data.meta[label])

	def annotate(self, ax, list_posx, list_pos_nuInu, labels, fontsize = 18, ha='center', pos_arrow =None, rotation=None,facecolors=None, edgecolors=None, colors=None):
		for i, lab in enumerate(labels):
			x = list_posx[i].to(self.unit_nu,equivalencies=u.spectral()).value
			equiv_nuInu = u.spectral_density(list_posx[i])
			y = list_pos_nuInu[i].to(self.unit_nuInu,equivalencies=equiv_nuInu).value
			bbox = None
			if (facecolors is not None) and (edgecolors is not None): bbox = dict(facecolor=facecolors[i], edgecolor=edgecolors[i], pad=5.0)
			c = 'k'
			if colors is not None: c=colors[i]
			if pos_arrow is None:
				ax.text(x, y, lab, fontsize=fontsize, ha=ha, color=c, rotation=rotation)#,bbox=bbox)
			else:
				xarr = pos_arrow[i][0].to(self.unit_nu,equivalencies=u.spectral()).value
				yarr = pos_arrow[i][1].to(self.unit_nuInu,equivalencies=equiv_nuInu).value				
				ax.annotate(lab, xy=(xarr, yarr), xytext=(x, y), fontsize=fontsize, ha=ha, color=c, arrowprops=dict(edgecolor=c, facecolor=c, arrowstyle="->",lw=1))
			
	def experiment_legend(self, ax, delta_y = 0.025, font_size = 16, reverse=False):
		ymin, ymax = self.main_axes['y_axis']['val']
		rpos_y = np.power(ymax/ymin,delta_y)
		for i, pos in enumerate(self.list_posx):
			pos_x = pos.to(self.unit_nu,equivalencies=u.spectral()).value
			if self.main_axes['x_axis']['val'][0] < pos_x < self.main_axes['x_axis']['val'][1]:
				if self.list_is_lower_legend[i]: pos_y0 = ymin*rpos_y**(1+len(self.list_labels[i]))
				else: pos_y0 = ymax/rpos_y**2
				list_lab = self.list_labels[i]
				list_col = self.list_colors[i]
				if reverse:
					list_lab = list(reversed(list_lab))
					list_col = list(reversed(list_col))			
				for j, label in enumerate(list_lab):
					pos_y = pos_y0/np.power(rpos_y,j)
					ax.text(pos_x, pos_y, label, color=list_col[j], fontsize=font_size)

	def write_caption_data(self, folder):
		def add_refs(ref_id):
			cite = ''
			unique_ref = list(set(ref_id))
			for i, ref in enumerate(unique_ref): 
				sep=','
				if i == len(unique_ref)-1: sep='}'
				cite+=str(ref)+sep
			return cite
		
		text = folder+" data points consist of "
		if len(self.ref_id_UL)>0: 
			sep = ', '
			if len(self.ref_id_M)+len(self.ref_id_IGL)==0: sep = '.'
			elif len(self.ref_id_M)==0 or len(self.ref_id_IGL)==0: sep = ' and '
			text += 'dark-patch estimates from \cite{'+add_refs(self.ref_id_UL)+ sep
		if len(self.ref_id_M)>0: 
			sep = ' and '
			if len(self.ref_id_IGL)==0: sep = '.'
			text += 'measurements from \cite{'+add_refs(self.ref_id_M)+ sep
		if len(self.ref_id_IGL)>0:
			sep = '.'
			text += 'galaxy counts from \cite{'+add_refs(self.ref_id_IGL)+ sep
				
		f = open('output_data/'+folder+'.tex',"w")
		f.write(text)
		f.close()
		
	def clean_ref_id(self):
		self.ref_id_M = []
		self.ref_id_IGL = []
		self.ref_id_UL = []

	def ecrb(self, ax, lower_legend=False, posx = 3*u.PeV, show_protons=False, show_all_particles=True, rescale_E=False):
		folder = 'ECRB'
		files = ['aartsen2019', 'bertaina2015', 'budnev2020', 'matthews2017', 'abreu2021']
		labels, colors = [], []
		for f in files:
			filename = os.path.join('data',folder,f+'.ecsv')
			data = QTable.read(filename, format='ascii.ecsv')
			#adapt energy scales of Auger and TA if requested
			if rescale_E:#Tsun+ '19
				label_rescale = r" - matched $E$-scale (Tsunesada+ '21)"
				Eth = 10*u.EeV 
				shift, stretch_per_decade = 0.045, 0.10
				if data.meta['observatory']=='Auger':
					E = (data['E'].to(Eth.unit)).value
					scaling = (1+shift)*np.ones_like(E)
					sel = data['E']>Eth
					scaling[sel] *= 1+stretch_per_decade*np.log10(E[sel]/Eth.value)
					data['E']*=scaling
					data.meta['observatory']+=label_rescale
					data.meta['label']+=label_rescale					
				elif data.meta['observatory']=='Telescope Array':			
					E = (data['E'].to(Eth.unit)).value
					scaling = (1-shift)*np.ones_like(E)
					sel = data['E']>Eth
					scaling[sel] *= 1-stretch_per_decade*np.log10(E[sel]/Eth.value)
					data['E']*=scaling
					data.meta['observatory']+=label_rescale
					data.meta['label']+=label_rescale					
			#retrieve the labels, set the colors
			color, label = self.plot_data(ax, data, alpha=0.0, keep_dataset=False)						
			if color!=None:
				labels.append(label)
				colors.append(color)
			if show_all_particles:
				#below the second knee
				sel = (data['E']<=100*u.PeV)
				self.plot_data(ax, data[sel], color=color, alpha=0.10, keep_dataset=False)						
				#above the second knee
				sel = (data['E']<=5*u.EeV)*(data['E']>100*u.PeV)
				self.plot_data(ax, data[sel], color=color, alpha=0.40)
				#above the ankle
				sel = data['E']>5*u.EeV 
				self.plot_data(ax, data[sel], color=color, alpha=1.0)				

		if show_protons:
			files_p = ['aartsen2019_p', 'arteaga2017_p', 'abreu2021_p']
			icolors_p = [0, 1, 4]#index of all-particle spectra corresponding to proton spectra
			for ifile, f in enumerate(files_p):
				filename = os.path.join('data',folder,f+'.ecsv')
				data = QTable.read(filename, format='ascii.ecsv')
				#retrieve the labels, set the colors
				index = icolors_p[ifile]
				color, label = self.plot_data(ax, data, color = colors[index], alpha=0.0, keep_dataset=False)
				labels[index] = label

				#below the knee
				sel = (data['E']<=3*u.PeV)
				self.plot_data(ax, data[sel], color=color, alpha=0.10, keep_dataset=False)						
				#below the proton ankle
				sel = (data['E']<=0.2*u.EeV)*(data['E']>3*u.PeV)
				self.plot_data(ax, data[sel], color=color, alpha=0.40, keep_dataset=False)
				#above the proton ankle
				sel = data['E']>0.2*u.EeV 
				self.plot_data(ax, data[sel], color=color, alpha=1.0)					

		self.list_is_lower_legend.append(lower_legend)				
		self.list_posx.append(posx)
		self.list_labels.append(labels)
		self.list_colors.append(colors)
		if self.is_write_caption_data: self.write_caption_data(folder)
		return colors[0]
		
	def enb(self, ax, lower_legend=False, posx = 20*u.GeV):
		folder = 'ENB'
		files = ['abbasi2021', 'abbasi2022' ]
		col = self.load_data(ax, lower_legend, posx, folder, files)
		if self.is_write_caption_data: self.write_caption_data(folder)		
		return col
		
	def cgb(self, ax, lower_legend=False, posx = 3*u.MeV):
		folder = 'CGB'
		files = ['weidenspointner2000', 'strong2004', 'ackermann2015' ]
		col = self.load_data(ax, lower_legend, posx, folder, files)
		if self.is_write_caption_data: self.write_caption_data(folder)		
		return col
		
	def cxb(self, ax, lower_legend=False, posx = 0.3*u.keV, include_Comptel=False):
		folder = 'CUB'
		files = [ 'stern1979', 'lieu1993', 'roberts2001']
		labels, colors = [], []	
		self.show_data(ax, labels, colors, folder, files)
		if self.is_write_caption_data: 
			self.write_caption_data(folder)				
			self.ref_id_M = []
			self.ref_id_IGL = []
			self.ref_id_UL = []		

		folder = 'CXB'
		files = [	'cappelluti2017', 'moretti2009', 'krivonos2021', 'deluca2004', 
					'revnivstev2003', 'ajello2008', 'tuerler2010', 'watanabe2000']
		if include_Comptel: files.append('weidenspointner2000')

		self.show_data(ax, labels, colors, folder, files)
		
		self.list_is_lower_legend.append(lower_legend)
		self.list_posx.append(posx)
		self.list_labels.append(labels)
		self.list_colors.append(colors)
		if self.is_write_caption_data: self.write_caption_data(folder)		
		
		return colors[0]	
		
	def cob(self, ax, lower_legend=False, posx = 1*u.mm):						
		folder = 'COB'
		files = [	'murthy1990', 'brown2000', 'murthy1989', 'martin1991', 
					'chiang2019', 'xu2005', 'milliard1992', 'edelstein2000',
					'windhorst2023','koushan2021', 'biteau2015_gamma_only',#'biteau2015',
					'lauer2022', 'mattila2017', 'matsuoka2011']
		col = self.load_data(ax, lower_legend, posx, folder, files)
		if self.is_write_caption_data: self.write_caption_data(folder)		
		return col
		
	def zodi(self, ax, lower_legend=False, posx = 0.3*u.um):						
		folder = 'ZL'
		files = [	'kawara2017', 'bernstein2007', 'matsuura2017',
					'korngut2022', 'sano2015', 'levenson2007',   
					'matsumoto2015', 'tsumura2013','sano2016', 'arendt2003'
					  ]	#ZL
		col = self.load_data(ax, lower_legend, posx, folder, files)
		if self.is_write_caption_data: self.write_caption_data(folder)	
			
		
	def cib(self, ax, lower_legend=False,posx = 500*u.m):
		folder = 'CIB'
		files = [	'odegard2019' ,'fujimoto2016', 'hsu2016', 'marsden2009', 
					'duivenvoorden2020', 'driver2016', 'juvela2009', 'penin2012',
					'odegard2007', 'matsuura2011',  'finkbeiner2000',
					'hopwood2010', 'altieri1999', 'clements1999'
					]
		labels, colors = [], []
		for f in files:
			filename = os.path.join('data',folder,f+'.ecsv')
			data = QTable.read(filename, format='ascii.ecsv')
			if f == 'driver2016':#redundant points with koushan2021 
				sel = (data['lambda']<0.35*u.um) + (data['lambda']>2.16*u.um)
				data = data[sel]
			color, label = self.plot_data(ax, data)
			if color!=None:
				labels.append(label)
				colors.append(color)
		
		self.list_is_lower_legend.append(lower_legend)
		self.list_posx.append(posx)
		self.list_labels.append(labels)
		self.list_colors.append(colors)
		if self.is_write_caption_data: self.write_caption_data(folder)	
		
		return colors[0]
		
	def crb(self, ax, lower_legend=False,posx = 500*u.m):
		folder = 'CRB'
		files = [	'dowell2018', 'vernstrom2011', 'hardcastle2021', 'vernstrom2014', 'zannoni2008', 'tompkins2023']
		col = self.load_data(ax, lower_legend, posx, folder, files)
		if self.is_write_caption_data: self.write_caption_data(folder)
		
		return col
