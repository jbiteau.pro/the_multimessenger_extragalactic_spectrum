import os
import matplotlib.pyplot as plt
plt.style.use('./allcrs.mplstyle')
import numpy as np
import emcee
import pandas as pd
import seaborn as sns
import copy
import os.path

import emcee
import pandas as pd
import seaborn as sns

from scipy import interpolate, signal

from astropy import units as u
from astropy import constants as const
from astropy.table import QTable, Table
from astropy.cosmology import FlatLambdaCDM

class TheFit():
   
	def __init__(self, main_axes, sample_filename = None, data=None, model_type="pwl", lg_nu_knots=None, init_with_model=None, cosmo = FlatLambdaCDM(H0=70, Om0=0.3), sec_axes=None, ms=8,lw=2):

		self.main_axes = main_axes
		self.unit_nu = main_axes['x_axis']['unit']
		self.unit_nuInu = main_axes['y_axis']['unit']		
		self.nuInu_crit = (cosmo.critical_density(0)*const.c**3/(4.*np.pi*u.sr)).to(self.unit_nuInu)

		self.sec_axes = sec_axes
		self.ms = ms
		self.lw = lw	

		if (sample_filename is None) and (data is None):
			print('sample_filename or data need to be defined !!!')
		
		elif sample_filename is not None:
			self.param_init = None
			self.read_model_from_file(sample_filename, '.ecsv')
			
		elif data is not None:
			data = data[:, data[0].argsort()]#sort as a function of frequency		
			self.data = data
			
			#Model setting
			self.model_type = model_type
			self.flat_samples = None	
			self.unit_nu_model = self.unit_nu
			self.unit_nuInu_model = self.unit_nuInu						
			if model_type=="spline":
				if init_with_model is None:
					#Select only measurements 
					sel = (data[2]<np.infty)*(data[3]<np.infty)
					init_data = []
					for i in range(4): init_data.append(data[i][sel])
					tck_spline_init = self.init_spline_with_data(init_data, np.min(lg_nu_knots), np.max(lg_nu_knots))
				else:
					tck_spline_init = self.init_spline_with_model(init_with_model, np.min(lg_nu_knots), np.max(lg_nu_knots))
									
				lg_nuInu_spline = interpolate.splev(lg_nu_knots, tck_spline_init)
				tck_spline = interpolate.splrep(lg_nu_knots, lg_nuInu_spline)			
				self.tck0 = tck_spline[0]
				self.tck2 = tck_spline[2]		
				self.param_init = tck_spline[1]
			elif model_type=="pwl":
				#init		
				sel = (data[2]<np.infty)
				init_data = (data.T[sel]).T
				x0, x1 = init_data[0][0], init_data[0][-1]
				y0, y1 = init_data[1][0], init_data[1][-1]
				
				self.lg_E0 = 0.5*np.log10(x0*x1)
				gamma_init = -np.log(y1/y0)/np.log(x1/x0)
				lg_phi0_init = 0.5*np.log10(y0*y1)
				self.param_init = (lg_phi0_init, gamma_init)
			elif model_type=="ssa_pwl":
				#select data above putative synchrotron self-absorption frequency
				E_ssa = (50*u.MHz).to(self.unit_nu,equivalencies=u.spectral()).value
				sel = (data[0]>E_ssa)*(data[2]<np.infty)
				init_data = (data.T[sel]).T
				
				#init		
				x0, x1 = init_data[0][0], init_data[0][-1]
				y0, y1 = init_data[1][0], init_data[1][-1]
						
				self.lg_E0 = 0.5*np.log10(x0*x1)
				gamma_init = -np.log(y1/y0)/np.log(x1/x0)
				lg_phi0_init = 0.5*np.log10(y0*y1)+np.log10(2)
				self.lg_E_ssa_min = np.log10((1*u.MHz).to(self.unit_nu,equivalencies=u.spectral()).value)					
				self.param_init = (lg_phi0_init, gamma_init, np.log10(E_ssa))			
			
		print ('Calling TheFit constructor')
	
	def FigSetup(self, Shape='Wide', credits=False):
		if Shape=='Wide':
			fig = plt.figure(figsize=(16.5,10.5))
			fudge_factor = 9.4
		elif Shape=='Rectangular':
			fig = plt.figure(figsize=(10.0,8))			
			fudge_factor = 6.8
		elif Shape=='Between':
			fig = plt.figure(figsize=(14.,10.5))
			fudge_factor = 9.6
						
		ax = fig.add_subplot(111)
		if credits: 
			normy = np.power(self.main_axes['y_axis']['val'][1]/self.main_axes['y_axis']['val'][0],1/fudge_factor)
			ax.text(self.main_axes['x_axis']['val'][1], self.main_axes['y_axis']['val'][0]/normy, "Credits: J. Biteau '23", fontsize=10, color='tab:gray', ha='right')		
		self.SetAxes(ax)
		return fig,ax
		
	def SetAxes(self, ax):
		ax.minorticks_off()
		#Set x-axis
		ax.set_xscale('log')
		ax.set_xlim(self.main_axes['x_axis']['val'])
		ax.set_xlabel(self.main_axes['x_axis']['title'])
		#Set y-axis
		ax.set_yscale('log')
		ax.set_ylim(self.main_axes['y_axis']['val'])
		ax.set_ylabel(self.main_axes['y_axis']['title'])
		#Set twin axes
		if self.sec_axes!=None:
			if self.sec_axes['x_axis']!=None:
				#Set x-axis	
				ax2 = ax.twiny()
				ax2.minorticks_off()
				ax2.set_xscale('log')
				ax2.set_xlabel(self.sec_axes['x_axis']['title'], labelpad=20)
				lim = np.array(self.main_axes['x_axis']['val'])*self.main_axes['x_axis']['unit']
				lim = lim.to(self.sec_axes['x_axis']['unit'], equivalencies=u.spectral()).value 
				ax2.set_xlim(lim)		
			if self.sec_axes['y_axis']!=None:
				#Set y-axis	
				ax3 = ax.twinx()
				ax3.minorticks_off()			
				ax3.set_yscale('log')
				ax3.set_ylabel(self.sec_axes['y_axis']['title'], labelpad=20)
				lim = np.array(self.main_axes['y_axis']['val'])*self.main_axes['y_axis']['unit']
				lim = lim.to(self.sec_axes['y_axis']['unit'], equivalencies=u.spectral()).value 
				ax3.set_ylim(lim)
				
	def spectral_quantiles(self, nu_range = None, x0 = None, quantiles = (0.16, 0.5, 0.84), rand_ln_nu = None, rand_ln_phi = None):
		if x0 is None:
			xmin = nu_range[0].to(self.unit_nu_model, equivalencies=u.spectral()).value
			xmax = nu_range[1].to(self.unit_nu_model, equivalencies=u.spectral()).value
			x0 = np.linspace(np.log10(xmin), np.log10(xmax),200)
		inds = np.random.randint(len(self.flat_samples), size=int(len(self.flat_samples)/10))
		
		#Randomize the energy-scale and flux to model systematic uncertainties on energy-scale
		nsamples = len(self.flat_samples)
		e_scale_factor = np.zeros(nsamples)
		phi_scale_factor = np.ones(nsamples)
		if rand_ln_nu is not None: e_scale_factor = rand_ln_nu*np.random.normal(size=nsamples)/np.log(10)
		if rand_ln_phi is not None: phi_scale_factor = np.exp(rand_ln_phi*np.random.normal(size=nsamples))		
		#Compute the quantiles
		array = [phi_scale_factor[i]*self.model(self.flat_samples[ind], x0+e_scale_factor[i]) for i, ind in enumerate(inds)]
		nuInu_q = np.quantile(array, quantiles, axis=0)
			
		return x0, nuInu_q[1], [nuInu_q[0], nuInu_q[-1]]

	def spectral_integral(self, nu_range, quantiles = (0.16, 0.5, 0.84), rand_ln_nu = None, rand_ln_phi = None):
		xmin = np.log10(nu_range[0].to(self.unit_nu_model, equivalencies=u.spectral()).value)
		xmax = np.log10(nu_range[1].to(self.unit_nu_model, equivalencies=u.spectral()).value)
		if xmin>xmax: xmin, xmax = xmax, xmin

		#Randomize the energy-scale and flux to model systematic uncertainties on energy-scale
		nsamples = len(self.flat_samples)
		e_scale_factor = np.zeros(nsamples)
		phi_scale_factor = np.ones(nsamples)
		if rand_ln_nu is not None: e_scale_factor = rand_ln_nu*np.random.normal(size=nsamples)
		if rand_ln_phi is not None: phi_scale_factor = np.exp(rand_ln_phi*np.random.normal(size=nsamples))		
		#Compute the integrals
		array = [phi_scale_factor[i]*self.model_integral(sample, e_scale_factor[i]+xmin, e_scale_factor[i]+xmax) for i, sample in enumerate(self.flat_samples)]
		array = np.array(array)*self.unit_nuInu_model
		array = array.to(self.unit_nuInu).value				
		int_dnuInu = np.quantile(array, quantiles, axis=0)*self.unit_nuInu
			
		return int_dnuInu[1], [int_dnuInu[0], int_dnuInu[-1]]

	def plot_dataset(self, ax, color=None, alpha=1, is_plot_init_model=False):
		#Define the frequency range for the model
		xmin, xmax = self.main_axes['x_axis']['val']
		x0 = np.linspace(np.log10(xmin), np.log10(xmax),200)
		nu0 = np.power(10, x0)
		
		if is_plot_init_model:
			nuInu0 = self.model(self.param_init, x0)
			ax.plot(nu0, nuInu0, "tab:gray", alpha=1, lw=3)	
		if self.flat_samples is not None:
			quantiles = (0.16, 0.5, 0.84)
			x0, nuInu_med, nuInu_bnds = self.spectral_quantiles(x0=x0, quantiles=quantiles)
			q0, q2 = quantiles[0], quantiles[-1]
			ax.fill_between(nu0, nuInu_bnds[0], nuInu_bnds[1], color="tab:gray", alpha=0.2, label=f"{q0}-{q2} confidence region")
			ax.plot(nu0, nuInu_med, color="tab:gray", alpha=1, lw=2, label="Median")
			
			#Plot the knots
			if self.model_type=="spline":
				f_interp = interpolate.interp1d(nu0, nuInu_med, fill_value="extrapolate")
				nu_knots = np.power(10, self.tck0)
				#print(self.tck0)
				ymin = self.main_axes['y_axis']['val'][0]
				for nu_k in nu_knots: 
					ax.plot([nu_k,nu_k], [ymin, f_interp(nu_k)], color='tab:red', alpha=0.5)
				
		#Plot the data
		nu = self.data[0]*self.unit_nu
		x = copy.copy(nu.to(self.main_axes['x_axis']['unit'],equivalencies=u.spectral()).value)
		
		y = self.data[1]*self.unit_nuInu
		y_errn = self.data[2]*self.unit_nuInu
		y_errp = self.data[3]*self.unit_nuInu
				
		equiv_nuInu = u.spectral_density(nu)
		y = y.to(self.main_axes['y_axis']['unit'],equivalencies=equiv_nuInu).value
		
		plt_y_errn = copy.copy(y_errn.to(self.main_axes['y_axis']['unit'],equivalencies=equiv_nuInu).value)
		plt_y_errp = copy.copy(y_errp.to(self.main_axes['y_axis']['unit'],equivalencies=equiv_nuInu).value)

		sel_circles = (y_errn!=np.infty)*(y_errp!=np.infty)
		sel_low_triangles = y_errn==np.infty
		sel_up_triangles = y_errp==np.infty
		plt_y_errn[plt_y_errn==np.infty]=0
		plt_y_errp[plt_y_errp==np.infty]=0		
		
		m = ['o','v','^']
		sels = [sel_circles, sel_low_triangles, sel_up_triangles]
		for i, sel in enumerate(sels):
			p = ax.plot(x[sel], y[sel], marker=m[i], ms = self.ms,fillstyle='none', linewidth=0, zorder=2, alpha=alpha, color=color)
			ax.errorbar(x[sel], y[sel], yerr=[plt_y_errn[sel],plt_y_errp[sel]], marker='', ms = self.ms, elinewidth=self.lw, linewidth=0, color = p[0].get_color(), zorder=1, alpha=alpha)

	#IO interface
	def read_model_from_file(self, save_flat_samples, ext):
		t = Table.read(save_flat_samples+ext, format="ascii"+ext)
		self.model_type = t.meta["model_type"]
		self.unit_nu_model = t.meta["nu_model_unit"]
		self.unit_nuInu_model = t.meta["nuInu_model_unit"]
		self.flat_samples = np.array([t[col] for col in t.colnames]).T
		if self.model_type=="spline":
			self.tck0 = t.meta["tck0"]
			self.tck2 = t.meta["tck2"]
		elif self.model_type=="pwl":
			self.lg_E0 = t.meta["lg_E0"]
		elif self.model_type=="ssa_pwl":
			self.lg_E0 = t.meta["lg_E0"]
			self.lg_E_ssa_min = t.meta["lg_E_ssa_min"]

	def write_model_to_file(self, save_flat_samples, ext):
		names = []
		meta = {}
		meta["model_type"] = self.model_type
		meta["nu_model_unit"] = self.unit_nu_model
		meta["nuInu_model_unit"] = self.unit_nuInu_model
		if self.model_type=="spline":
			meta["tck0"] = self.tck0
			meta["tck2"] = self.tck2
			names = [f"w{i}" for i in range(len(self.tck0))]
		elif self.model_type=="pwl":
			meta["lg_E0"] = self.lg_E0
			names = ["lg_phi0", "gamma"]
		elif self.model_type=="ssa_pwl":
			meta["lg_E0"] = self.lg_E0
			meta["lg_E_ssa_min"] = self.lg_E_ssa_min
			names = ["lg_phi0", "gamma", "lg_E_ssa"]	
		t = Table(self.flat_samples, names=names, meta = meta)
		t.write(save_flat_samples+ext, overwrite=True)
		
	#Generic model
	def model(self, theta, x):
		if self.model_type=="spline": return self.spline_model(theta, x)
		elif self.model_type=="pwl": return self.pwl_model(theta, x)
		elif self.model_type=="ssa_pwl": return self.ssa_pwl_model(theta, x)		
		return None
		
	def model_integral(self, theta, xmin, xmax, n=100, is_log10=False, is_critical_units=False):
		lg10_nu = np.linspace(xmin, xmax, n)
		nuInu = self.model(theta, lg10_nu)
		integral = np.trapz(y=nuInu, x=lg10_nu*np.log(10))
		if is_critical_units: integral/=self.nuInu_crit.value
		if is_log10: return np.log10(integral)
		return integral

	#Spline-related functions		
	def init_spline_with_model(self, filename, lg_nu_min, lg_nu_max):
		data = QTable.read(filename, format='ascii.ecsv')
		nu = data.columns[0].to(self.unit_nu, equivalencies=u.spectral())
		nuInu = data.columns[1].to(self.unit_nuInu, equivalencies=u.spectral())
		lg_nu_init, lg_nuInu_init = np.log10(nu.value), np.log10(nuInu.value)
		f_interp = interpolate.interp1d(lg_nu_init, lg_nuInu_init, fill_value="extrapolate")
		lg_nu = np.linspace(lg_nu_min, lg_nu_max,100)

		return interpolate.splrep(lg_nu, f_interp(lg_nu))

	def init_spline_with_data(self, init_data, lg_nu_min, lg_nu_max):
		#Smooth
		def moving_average(a, n=3) :
			ret = np.cumsum(a, dtype=float)
			ret[n:] = ret[n:] - ret[:-n]
			return ret[n - 1:] / n			
		lg_x = moving_average(np.log10(init_data[0]))
		lg_y = moving_average(np.log10(init_data[1]))
		f_interp = interpolate.interp1d(lg_x, lg_y, fill_value="extrapolate")
		lg_nu = np.linspace(lg_nu_min, lg_nu_max,100)
		
		return interpolate.splrep(lg_nu, f_interp(lg_nu))

	def spline_model(self, tck1, x):
		tck = (self.tck0, tck1, self.tck2)
		return np.power(10,interpolate.splev(x, tck))
	
	def inflection_points(self, lg10_nu, lg10_nuInu, ranges):

		def load_derivative(x, y):
			dx = x[1:]-x[:-1]
			dy = y[1:]-y[:-1]
			return 0.5*(x[1:]+x[:-1]), dy/dx 
		
		lg10_nu1, lg10_nuInu1 = load_derivative(lg10_nu, lg10_nuInu)
		lg10_nu2, lg10_nuInu2 = load_derivative(lg10_nu1, lg10_nuInu1)		
		i_infl2 = signal.find_peaks(np.abs(lg10_nuInu2))[0]
		lg10_nu2_infl = lg10_nu2[i_infl2]
		bounds = [None, None, None]
		for i, r in enumerate(ranges):
			sel = (lg10_nu2_infl>r[0])*(lg10_nu2_infl<r[1])
			if np.sum(sel)>0: bounds[i] = np.median(lg10_nu2_infl[sel])
		return bounds

	#PWL-related functions		
	def pwl_model(self, theta, x):
		lg_phi0, gamma = theta
		return np.power(10, lg_phi0-gamma*(x-self.lg_E0))

	def ssa_pwl_model(self, theta, x):
		lg_phi0, gamma, lg_E_ssa = theta
		index_nuInu_ssa = 1+5/2
		pow_ssa = np.power(10, -(gamma+index_nuInu_ssa)*(x-lg_E_ssa))
		theta_pwl = (lg_phi0, gamma)
		return self.pwl_model(theta_pwl, x)/(1+pow_ssa)

	#MCMC sampler
	def run_sampler(self, nsamples = 100000, nwalkers=128, verbose=False, save_flat_samples=None, autocorr_time = 2000, ext=".ecsv"):
	
		def log_probability(theta, x, y, yerr_m, yerr_p):

			def log_prior(theta, min_theta=-10, max_theta=5):
				is_out_range = np.sum((theta>max_theta)+(theta<min_theta))
				if self.model_type=="ssa_pwl": is_out_range += (theta[-1]<self.lg_E_ssa_min)
				if is_out_range: return -np.infty
				return 0
				
			def log_likelihood(theta, x, y, yerr_m, yerr_p):
				model = self.model(theta, np.log10(x))
				is_model_below_data = model<y
				is_model_above_data = np.invert(is_model_below_data)
				sigma2 = np.ones_like(y)
				sigma2[is_model_below_data] = yerr_m[is_model_below_data]**2 
				sigma2[is_model_above_data] = yerr_p[is_model_above_data]**2 		
				return -0.5 * np.sum((y - model)** 2 / sigma2)
				
			lp = log_prior(theta)
			if not np.isfinite(lp): return -np.inf
			return lp + log_likelihood(theta, x, y, yerr_m, yerr_p)
		
		if (save_flat_samples is not None) and os.path.isfile(save_flat_samples+ext):
			self.read_model_from_file(save_flat_samples, ext=ext)
		else:
			x, y, yerr_m, yerr_p = self.data
			init = self.param_init
			ndim = len(init)
			pos = np.array(init) + 1e-4 * np.random.randn(nwalkers, ndim)
			sampler = emcee.EnsembleSampler(nwalkers, ndim, log_probability, args=(x, y, yerr_m, yerr_p))
			sampler.run_mcmc(pos, nsamples, progress=True);

			tau = autocorr_time
			if verbose: 
				tau = np.median(sampler.get_autocorr_time())
				print("Autocorrelation time:", tau)
			self.flat_samples = sampler.get_chain(discard=int(3*tau), thin=int(tau/2), flat=True)
			if save_flat_samples is not None: 
				self.write_model_to_file(save_flat_samples, ext)

	#Plot parameters
	def chain_plot(self, trace, labels=None, save=None, name="Chain plots of parameters"):
		trace = np.array(trace).T
		fig, axes = plt.subplots(len(labels), figsize=(10, 10), sharex=True)
		fig.suptitle(name)
		if len(labels)==1: axes = [axes]
		for i, lab in enumerate(labels):
			ax = axes[i]
			ax.plot(trace[i], "k", alpha=0.1, lw=1)
			ax.set_xlim(0, len(trace[i]))
			ax.set_ylabel(lab)
		axes[-1].set_xlabel("step number");
		if len(labels)>1: fig.align_ylabels(axes.flatten())
		if save is not None:
			plt.savefig("output_fit/{}_chain.png".format(save), bbox_inches="tight")
			plt.savefig("output_fit/{}_chain.pdf".format(save), bbox_inches="tight")		
			plt.close()	
						
	def corner_plot(self, trace, labels=None, confidence=[0.68, 0.95], save=None, bins=50, name="Corner plots of parameters"):

		def corner_contour(trace1, trace2, levels=[0.68, 0.95], colors=["tab:blue", "skyblue"], **kwargs):

			def sigma_level(trace1, trace2, nbins=20):
				L, xbins, ybins = np.histogram2d(trace1, trace2, nbins)
				shape = L.shape
				L = L.ravel()
				i_sort = np.argsort(L)[::-1]
				i_unsort = np.argsort(i_sort)
				
				L = L[i_sort].cumsum()
				L = L/L[-1]
					
				return 0.5*(xbins[1:]+xbins[:-1]), 0.5*(ybins[1:]+ybins[:-1]), L[i_unsort].reshape(shape)
					
			plt.hexbin(trace1, trace2, **kwargs)
			xbins, ybins, sigma = sigma_level(trace1, trace2)
			plt.contour(xbins, ybins, sigma.T, levels=levels, colors=colors)
					
		def diag_contour(trace, levels=[0.68, 0.95], bins=20, colors=["tab:blue", "skyblue"], **kwargs):
			levels=levels[::-1]
			colors=colors[:len(levels)][::-1]
			L, xbins = np.histogram(trace, bins=bins)
			M = L.copy()
			i_sort = np.argsort(L)[::-1]
			i_unsort = np.argsort(i_sort)
			L = L[i_sort].cumsum()
			L = L/L[-1]
			L = L[i_unsort]
			xbins = 0.5*(xbins[1:]+xbins[:-1])
			plt.hist(trace, bins=bins, **kwargs)
			for l, c in zip(levels, colors):
				plt.fill_between(xbins[L<l], M[L<l], color=c, step="mid")
				plt.axvline(x=np.percentile(trace, 50*(1+l)), color="k", linestyle="--", label="{}%".format(l*100))
				plt.axvline(x=np.percentile(trace, 50*(1-l)), color="k", linestyle="--")
				plt.axvline(x=np.median(trace), color="red", label="median")

		df = pd.DataFrame(trace, columns=labels)
		with sns.axes_style('ticks'):
			grid = sns.PairGrid(df, corner=True, height=5)
			plt.subplots_adjust(top=0.9)
			grid.fig.suptitle(name)
			grid.map_lower(corner_contour, levels=confidence, gridsize=50, linewidths=0, cmap="Blues")
			grid.map_diag(diag_contour, levels=confidence, bins=bins, alpha=0.5)
		if save is not None:
			plt.savefig("output_fit/{}_corner.png".format(save), bbox_inches="tight")
			plt.savefig("output_fit/{}_corner.pdf".format(save), bbox_inches="tight")		
			plt.close()

	def plot_chain_and_corner(self, boundaries, labels, is_log10=False, is_critical_units=False, save="MWL_EGAL_Bckgds"):
		
		if self.flat_samples is not None:
			array = [[self.model_integral(sample, b[0], b[1], is_log10=is_log10, is_critical_units=is_critical_units) for b in boundaries] for sample in self.flat_samples]
			array = np.array(array)*self.unit_nuInu_model
			array = array.to(self.unit_nuInu).value
			name = ""
			if is_log10: name+= r"log$_{10}$ "
			if is_critical_units: name+= r"$\Omega$"
			else: name+= r"$\nu$I$_\nu$"
			self.corner_plot(array, labels, save=save, name = name)
			self.chain_plot(array, labels, save=save, name = name)	
	
	def plot_components(self, nu_bounds, labels, save="MWL_EGAL_Bckgds"):
			
		def check_boundaries(labels, nu_min, nu_max, nu_bnds, dlog10_nu_bnds = 1.0, save="MWL_EGAL_Bounds"):	
		 		
			#Range of the MWL EGAL spectrum
			xmin = np.log10(nu_min.to(self.unit_nu, equivalencies=u.spectral()).value)
			xmax = np.log10(nu_max.to(self.unit_nu, equivalencies=u.spectral()).value)		

			#Search ranges for inflection points
			x_bnds = np.array([np.log10(nu.to(self.unit_nu, equivalencies=u.spectral()).value) for nu in nu_bnds])
			x0_bnds, x1_bnds= x_bnds-dlog10_nu_bnds, x_bnds+dlog10_nu_bnds
			ranges = list(np.array([x0_bnds, x1_bnds]).T)

			list_nuInu = []
			if self.flat_samples is not None:
				lg10_nu = np.linspace(xmin, xmax, 1000)
				list_nuInu = [self.spline_model(sample, lg10_nu) for sample in self.flat_samples]
				med_nuInu = np.median(list_nuInu, axis=0)
				list_lg_nu = self.inflection_points(lg10_nu, med_nuInu, ranges)
				for i, nu in enumerate(nu_bnds):
					nu_out = np.power(10,list_lg_nu[i])*self.unit_nu
					nu_out = nu_out.to(nu.unit, equivalencies=u.spectral())
					print("Boundary "+labels[i]+":", nu_out)
		
		#Check the boundaries
		if self.model_type=="spline": 
			lab = [labels[i]+" - "+labels[i+1] for i in range(len(labels)-1)]	
			check_boundaries(lab, nu_bounds[0], nu_bounds[-1], nu_bounds[1:-1])
		
		#Show the best-fit integrals
		boundaries = []
		for i, lab in enumerate(labels): 
			lg_nu0 = np.log10(nu_bounds[i].to(self.unit_nu_model, equivalencies=u.spectral()).value)
			lg_nu1 = np.log10(nu_bounds[i+1].to(self.unit_nu_model, equivalencies=u.spectral()).value)	
			boundaries.append(np.sort([lg_nu0, lg_nu1]))
		self.plot_chain_and_corner(boundaries, labels, save=save, is_log10=True, is_critical_units=False)
	
