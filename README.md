[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7842239.svg)](https://doi.org/10.5281/zenodo.7842239)

## The Multi-Messenger Extragalactic Spectrum (2023)

Please email me [biteau(at)in2p3.fr] or make a pull request if you would like any dataset being added.

I'm pleased to acknowledge the data compilation efforts by:
*  Ryley Hill ([Hill et al., 2018](https://ui.adsabs.harvard.edu/abs/2018ApSpe..72..663H))
*  Carmelo Evoli ([Evoli, 2020](https://doi.org/10.5281/zenodo.1468852))

The structure of the code is strongly inspired by that developed by Carmelo Evoli for his [Cosmic-Ray Energy Spectrum](https://github.com/carmeloevoli/The_CR_Spectrum).

### Install and Run

> git clone https://gitlab.com/jbiteau.pro/the_multimessenger_extragalactic_spectrum.git

> conda env create -f the_multimessenger_extragalactic_spectrum/environment.yml

> conda activate the_egal_spec

> cd the_multimessenger_extragalactic_spectrum/

> python The_EGAL_spectrum.py

_Note: texlive needed for rendering_
- MAC 
  > brew install texlive
- Debian-based 
  > sudo apt-get install texlive-latex-extra texlive-fonts-recommended dvipng cm-super


### Spectral Data

<img src="The_MM_EGAL_Background_2023.png" width="800">

Download: [png](https://gitlab.com/jbiteau.pro/the_multimessenger_extragalactic_spectrum/-/blob/master/The_MM_EGAL_Background_2023.png), [pdf](https://gitlab.com/jbiteau.pro/the_multimessenger_extragalactic_spectrum/-/blob/master/The_MM_EGAL_Background_2023.pdf)

### Spectral Fit

<img src="The_Fitted_MM_EGAL_Background_2023.png" width="800">

Download: [png](https://gitlab.com/jbiteau.pro/the_multimessenger_extragalactic_spectrum/-/blob/master/The_Fitted_MM_EGAL_Background_2023.png), [pdf](https://gitlab.com/jbiteau.pro/the_multimessenger_extragalactic_spectrum/-/blob/master/The_Fitted_MM_EGAL_Background_2023.pdf)


### How to cite it

```
@misc{The_MM_EGAL_spectrum,
  author       = {Biteau, Jonathan},
  title        = {The Multi-Messenger Extragalactic Spectrum},
  month        = april,
  year         = 2023,
  publisher    = {Zenodo},
  doi          = {10.5281/zenodo.1468852},
  url          = {https://doi.org/10.5281/zenodo.1468852}
}
```
