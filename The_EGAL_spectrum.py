from astropy import units as u
from astropy import constants as const
import matplotlib.lines as mlines
import numpy as np
from scipy import interpolate
from PlotFuncs import TheSpectrum, MySaveFig
from FitFuncs import TheFit

def round_to_sig_digit(x, ex_ref, sig_ex_ref= 1):
	def pos_dot(x):
		return int(np.floor(np.log10(abs(x))))+1
	def sig_digit(x, ex, sig_ex):
		return pos_dot(x)-pos_dot(ex)+sig_ex

	ex_ref = round(ex_ref, sig_ex_ref-pos_dot(ex_ref))
	sig_x = sig_digit(x, ex_ref, sig_ex_ref)
	prec = sig_x-pos_dot(x)
	r = round(x, prec)
	if(prec<0): prec=0
	return f"{r:.{prec}f}"

def format_with_uncertainties(data):
	x, ex_l, ex_h = data
	u_ref = x.unit

	a0 = x.to(u_ref).value
	b0 = ex_l.to(u_ref).value
	c0 = ex_h.to(u_ref).value	
	ex_ref = min(b0, c0)	

	a = round_to_sig_digit(a0, ex_ref)
	b = round_to_sig_digit(b0, ex_ref)
	c = round_to_sig_digit(c0, ex_ref)	

	return r"$"+a+"_{-"+b+"}^{+"+c+"}$"+"\,"+f"{u_ref:latex}"


def The_MM_EGAL_Background(is_direct=True, is_measurement=True, is_count=True, ms=8):
	#Principal unit system
	x_axis = dict(	unit = u.Hz, 
					val = [1E5,1E36],
					title= r'Frequency, $\nu$ [Hz]')
	y_axis = dict(	unit = u.nW/(u.m**2*u.sr), 
					val = [0.3E-7, 1E3],
					title= r'$\nu I_\nu$ [nW m$^{-2}$ sr$^{-1}$]')
	principal_unit_system = dict(x_axis = x_axis, y_axis = y_axis)

	#Secondary unit system
	x_axis = dict(	unit = u.MeV, 
					title= r'Energy, $E$ [MeV]')
	y_axis = dict(	unit = u.GeV/(u.m**2*u.s*u.sr),
					title= r'$E^{2} {\rm d}J/{\rm d}E$ [GeV m$^{-2}$ s$^{-1}$ sr$^{-1}$]')
	secondary_unit_system = dict(x_axis = x_axis, y_axis = y_axis)

	#Set the figure
	plot = TheSpectrum(main_axes = principal_unit_system, sec_axes = secondary_unit_system, ms = ms, lw = 2)
	fig, ax = plot.FigSetup()

	#Plot model
	plot.model_all(ax)

	#Plot points
	colECRB = plot.ecrb(ax, lower_legend=True)
	colENB = plot.enb(ax, lower_legend=True)
	colCGB = plot.cgb(ax, lower_legend=True)
	colCXB = plot.cxb(ax, lower_legend=True)
	colCOB = plot.cob(ax, lower_legend=True)
	colCIB = plot.cib(ax, lower_legend=False)
	colCRB = plot.crb(ax, lower_legend=True)

	#Experimental legend
	plot.experiment_legend(ax, font_size=14)

	#Annotations
	labels = [	r'CRB', r'CIB', r'COB', r'CXB', r'CGB', r'ENB', r'ECRB']
	list_posx = [	0.05*u.GHz, 155*u.um, 0.9*u.um, 20*u.keV, 1*u.GeV, 0.2*u.PeV, 5*u.EeV]	
	list_pos_nuInu = 1.5E-2*np.ones(len(labels))*u.nW/(u.m**2*u.sr)
	plot.annotate(ax, list_posx, list_pos_nuInu, labels)

	#Legend
	handles, labels = ax.get_legend_handles_labels()
	if is_direct: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='v', label='Dark patch')) 
	if is_measurement: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='o', label='Measurement')) 
	if is_count: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='^', label='Galaxy count')) 	
	ax.legend(handles=handles, loc='upper right', labelspacing = 0.3, fontsize = 18)

	#Save
	MySaveFig(fig, 'The_MM_EGAL_Background_2023')
	
	return colECRB,	colENB, colCGB, colCXB, colCOB, colCIB, colCRB


def The_Fitted_MM_EGAL_Background():
				
	def PlotResults(plot, ax, spectral_freq, spectral_curves, spectral_bounds, colors, joint_ranges=None, nuInu_integrals=None, indices_UHECR_AllSubAnkle_pSubAnkle_AllSupraAnkle=None):
		def sort_and_convert(data):
			data[0] = data[0].to(plot.unit_nu, equivalencies=u.spectral()).value
			data[1] = data[1].to(plot.unit_nuInu).value
			data = np.array(data)
			return data[:, data[0].argsort()]#sort as a function of frequency	
			
		def my_interp(data0, data1):
			data0 = sort_and_convert(data0)
			data1 = sort_and_convert(data1)			
			if data0[0][-1]<data1[0][0]:
				xmin, xmax = data0[0][-1], data1[0][0]
				xp = np.concatenate((data0[0], data1[0]))
				yp = np.concatenate((data0[1], data1[1]))
			else:
				xmin, xmax = data1[0][-1], data0[0][0]			
				xp = np.concatenate((data1[0], data0[0]))
				yp = np.concatenate((data1[1], data0[1]))
			x = np.logspace(np.log10(xmin), np.log10(xmax),10)	
			f = interpolate.interp1d(np.log(xp), np.log(yp), kind='cubic')
			y = np.exp(f(np.log(x)))
			return x*plot.unit_nu, y*plot.unit_nuInu
		
		nuInu_min = plot.main_axes['y_axis']['val'][0]*plot.unit_nuInu#for hatched regions
		for i, freq in enumerate(spectral_freq): 
			if nuInu_integrals is not None:
				plot.plot_line(ax, freq, nuInu_integrals[i][0]*np.ones_like(freq.value)/np.log(freq[-1]/freq[0]), color=colors[i], lw=2, ls="-.")
			hatch = "/"
			nuInu_min = plot.main_axes['y_axis']['val'][0]*plot.unit_nuInu#for hatched regions
			if indices_UHECR_AllSubAnkle_pSubAnkle_AllSupraAnkle is not None:	
				iAllSub = indices_UHECR_AllSubAnkle_pSubAnkle_AllSupraAnkle[0]
				ipSub = indices_UHECR_AllSubAnkle_pSubAnkle_AllSupraAnkle[1]			
				iAllSup = indices_UHECR_AllSubAnkle_pSubAnkle_AllSupraAnkle[2]
				if i== iAllSub: nuInu_min = spectral_bounds[ipSub][1]	
			plot.plot_range(ax, freq, nuInu_min*np.ones_like(freq.value), spectral_bounds[i][1], edgecolor=colors[i], color=colors[i], lw=0, hatch=hatch, alpha=0.2)
			plot.plot_range(ax, freq, spectral_bounds[i][0], spectral_bounds[i][1], edgecolor=colors[i], color=colors[i], lw=1, alpha=0.3)
			plot.plot_line(ax, freq, spectral_curves[i], color=colors[i], lw=2)
		if joint_ranges is not None:
			for bnds_index in joint_ranges:
				#central curve
				freq0, nuInu0 = spectral_freq[bnds_index[0]], spectral_curves[bnds_index[0]]
				freq1, nuInu1 = spectral_freq[bnds_index[1]], spectral_curves[bnds_index[1]]
				freq, nuInu = my_interp([freq0, nuInu0], [freq1, nuInu1])
			
				#bounds
				nuInu0_low, nuInu1_low = spectral_bounds[bnds_index[0]][0], spectral_bounds[bnds_index[1]][0]
				tmp, nuInu_low = my_interp([freq0, nuInu0_low], [freq1, nuInu1_low])
				nuInu0_high, nuInu1_high = spectral_bounds[bnds_index[0]][1], spectral_bounds[bnds_index[1]][1]
				tmp, nuInu_high = my_interp([freq0, nuInu0_high], [freq1, nuInu1_high])				

	
				#plot
				hatch = ".."
				nuInu_min = plot.main_axes['y_axis']['val'][0]*plot.unit_nuInu#for hatched regions		
				plot.plot_range(ax, freq, nuInu_min*np.ones_like(freq.value), nuInu_high, edgecolor="tab:gray", color="tab:gray", lw=0, hatch=hatch, alpha=0.2)				
				plot.plot_line(ax, freq, nuInu, color="tab:gray", lw=2, ls="--")				
				plot.plot_range(ax, freq, nuInu_low, nuInu_high, color="tab:gray", edgecolor="tab:gray", alpha=0.3, lw=1)

	def FitResults(filenames, nu_ranges, principal_unit_system, rand_ln_nu, rand_ln_phi, indices_UHECR_AllSubAnkle_pSubAnkle_AllSupraAnkle=None):
		spectral_freq, spectral_curves, spectral_bounds, nuInu_integrals = [], [], [], []
		for i, filename in enumerate(filenames):
			fit = TheFit(sample_filename='output_fit/'+filename, main_axes=principal_unit_system)
			for nu_range in nu_ranges[i]:
				#Median curves and bounds
				lg_freq, curves, bounds = fit.spectral_quantiles(nu_range, rand_ln_nu=rand_ln_nu[i], rand_ln_phi=rand_ln_phi[i])
				spectral_freq.append(np.power(10,lg_freq)*fit.unit_nu_model)
				spectral_curves.append(curves*fit.unit_nuInu_model)
				spectral_bounds.append(bounds*fit.unit_nuInu_model)
				
				#Integrals and uncertainties
				int_dnuInu, bounds_int = fit.spectral_integral(nu_range, rand_ln_nu=rand_ln_nu[i], rand_ln_phi=rand_ln_phi[i])
				nuInu_integrals.append([int_dnuInu, int_dnuInu-bounds_int[0], bounds_int[1]-int_dnuInu])
				
		if indices_UHECR_AllSubAnkle_pSubAnkle_AllSupraAnkle is not None:	
			iAllSub = indices_UHECR_AllSubAnkle_pSubAnkle_AllSupraAnkle[0]
			ipSub = indices_UHECR_AllSubAnkle_pSubAnkle_AllSupraAnkle[1]			
			iAllSup = indices_UHECR_AllSubAnkle_pSubAnkle_AllSupraAnkle[2]			
						
			#Hillas B = iAllSub-ipSub
			if nuInu_integrals[iAllSub][0].unit is nuInu_integrals[ipSub][0].unit:
				unit_nuInu = nuInu_integrals[iAllSub][0].unit
				nuInu_integrals[iAllSub][0] -= nuInu_integrals[ipSub][0]#central value
				array_unc_l = [nuInu_integrals[iAllSub][1].value, nuInu_integrals[ipSub][2].value]
				nuInu_integrals[iAllSub][1] = np.sqrt(np.sum(np.array(array_unc_l)**2,axis=0))*unit_nuInu
				array_unc_h = [nuInu_integrals[iAllSub][2].value, nuInu_integrals[ipSub][1].value]
				nuInu_integrals[iAllSub][2] = np.sqrt(np.sum(np.array(array_unc_h)**2,axis=0))*unit_nuInu
			else:
				print("Sub-ankle proton and all-particle spectra have different units !!")
				
		return spectral_freq, spectral_curves, spectral_bounds, nuInu_integrals			
	#Principal unit system
	x_axis = dict(	unit = u.Hz, val = [1E5,1E36],		
					title= r'Frequency, $\nu$ [Hz]')
	y_axis = dict(	unit = u.nW/(u.m**2*u.sr), val = [0.3E-7, 1E3],
					title= r'$\nu I_\nu$ [nW m$^{-2}$ sr$^{-1}$]')
	principal_unit_system = dict(x_axis = x_axis, y_axis = y_axis)

	#Secondary unit system
	x_axis = dict(	unit = u.MeV, 
					title= r'Energy, $E$ [MeV]')
	y_axis = dict(	unit = u.GeV/(u.m**2*u.s*u.sr),
					title= r'$E^{2} {\rm d}J/{\rm d}E$ [GeV m$^{-2}$ s$^{-1}$ sr$^{-1}$]')
	secondary_unit_system = dict(x_axis = x_axis, y_axis = y_axis)

	#Set the figure
	plot = TheSpectrum(main_axes = principal_unit_system, sec_axes = secondary_unit_system, lw = 2)
	fig, ax = plot.FigSetup()

	#Plot CMB
	plot.model_cmb(ax)

	#Retrieve fit models and integrals for EBL and ENB
	nu_crb_min, nu_crb_max = 10*u.MHz, 10*u.GHz
	nu_cob_min, nu_cob_max, nu_cib_max = 0.1*u.um, 10*u.um, 2.0*u.mm
	nu_cxb_min, nu_cxb_max, nu_cgb_max = 0.3*u.keV, 2*u.MeV, 1*u.TeV
	nu_enb_min, nu_enb_max = 30*u.TeV, 3*u.PeV
	nu_ranges = [	[[nu_crb_min, nu_crb_max]], 
					[[nu_cob_max, nu_cib_max], [nu_cob_min, nu_cob_max]],
					[[nu_cxb_min, nu_cxb_max], [nu_cxb_max, nu_cgb_max]],
					[[nu_enb_min, nu_enb_max]]
				]#integrals to be computed
	filenames = ['crb_samples', 'ebl_samples', 'cxbcgb_samples', 'enb_samples']
	colors = ['tab:brown', 'tab:red', 'tab:blue', 'tab:orange', 'tab:green', 'tab:cyan']
	rand_ln_phi= [None]*len(filenames)
	rand_ln_phi[-2]=0.1#assumed to be 10% for both CXB = (XRT-NuStar stdev at 5 keV) and CGB  = (20% sys. on iso due to Gal. foreground)*(50% iso contribution)
	rand_ln_nu= [None]*len(filenames)
	rand_ln_nu[-1]=0.15#assumed to be 15% for IceCube as UHECRs
	spectral_freq, spectral_curves, spectral_bounds, nuInu_integrals = FitResults(filenames, nu_ranges, principal_unit_system, rand_ln_nu=rand_ln_nu, rand_ln_phi=rand_ln_phi)

	#Join the microwave and UV ranges
	index_crb, index_cib = 0, 1
	index_cob, index_cxb = 2, 3
	joint_ranges = [[index_crb, index_cib], [index_cob, index_cxb]]

	#Plot EBL and ENB
	PlotResults(plot, ax, spectral_freq, spectral_curves, spectral_bounds, colors, joint_ranges, nuInu_integrals=None)
	
	#Retrieve fit models and integrals for ECRB
	Ep_min, Enucl_min, Enucl_max = 0.2*u.EeV, 5*u.EeV, 200*u.EeV
	#Zmean = 4#max inferred at 10 EeV - 1709.07321
	#Ep_min, Enucl_min, Enucl_max = Enucl_min/Zmean, 5*u.EeV, 200*u.EeV
	nu_ranges_UHECR = 	[	[[Ep_min, Enucl_min]],
							[[Ep_min, Enucl_min], [Enucl_min, Enucl_max]]
						]#integrals to be computed
	filenames_UHECR = ['ecrb_protons_samples', 'ecrb_allpart_samples']	
	colors_UHECR = ['tab:pink', 'tab:olive', 'tab:purple']
	ind_allpsub_sup = [1,0,2]
	rand_ln_phi= [None]*len(filenames_UHECR)
	rand_ln_nu= [0.15]*len(filenames_UHECR)#15% for UHECRs
	spectral_freq_UHECR, spectral_curves_UHECR, spectral_bounds_UHECR, nuInu_integrals_UHECR = FitResults(filenames_UHECR, nu_ranges_UHECR, principal_unit_system, rand_ln_nu=rand_ln_nu, rand_ln_phi=rand_ln_phi, indices_UHECR_AllSubAnkle_pSubAnkle_AllSupraAnkle=ind_allpsub_sup)#rand_ln_nu: 20% systematic shift in the energy scale
	
	#Plot ECRB
	PlotResults(plot, ax, spectral_freq_UHECR, spectral_curves_UHECR, spectral_bounds_UHECR, colors_UHECR, nuInu_integrals=None, indices_UHECR_AllSubAnkle_pSubAnkle_AllSupraAnkle=ind_allpsub_sup)

	#Annotation for CMB
	posx_CMB, posy_CMB = 100*u.GHz, 2.5*u.nW/(u.m**2*u.sr)
	textCMB, colCMB = 'CMB', 'gray'
	plot.annotate(ax, [posx_CMB], [posy_CMB], [textCMB], colors=[colCMB])
		
	#Annotations for MM egal backgds
	def text_range(w_range):		
		a, b = w_range
		prec_a, prec_b = 0, 0
		if a.value<1: prec_a=1
		if b.value<1: prec_b=1
		val_a, val_b, unit_a, unit_b = f"{a.value:.{prec_a}f}", f"{b.value:.{prec_b}f}", "", f"{b.unit:latex}"
		if a.unit != b.unit: unit_a = f"{a.unit:latex}"
		if "mu" in unit_a: unit_a = unit_a.replace("\mathrm{\mu",r"\mu\mathrm{")
		if "mu" in unit_b: unit_b = unit_b.replace("\mathrm{\mu",r"\mu\mathrm{")
		sep_vu, sep_ab = r"$\,$", r"$\,-\,$"		
		return val_a+sep_vu+unit_a+sep_ab+val_b+sep_vu+unit_b
	nu_ranges_all =	nu_ranges+[[[Ep_min, Enucl_max]]]
	lab_ranges = [text_range(item) for sublist in nu_ranges_all for item in sublist]
	labels = [	r'CRB', r'CIB', r'COB', r'CXB', r'CGB', r'ENB', r'ECRB']
	list_posx = [	0.05*u.GHz, 155*u.um, 0.9*u.um, 20*u.keV, 1*u.GeV, 0.2*u.PeV, 5*u.EeV]	
	
	list_pos_nuInu = 1.5E-2*np.ones(len(labels))*u.nW/(u.m**2*u.sr)
	plot.annotate(ax, list_posx, list_pos_nuInu, labels, colors=colors + [colors_UHECR[-1]] )
	plot.annotate(ax, list_posx, list_pos_nuInu/1.8, lab_ranges, colors=colors + [colors_UHECR[-1]], fontsize=11)	
	
	text_ankle = f"{Enucl_min.value:.0f}"+r"$\,$"+f"{Enucl_min.unit:latex}"
	posx_ankle = Enucl_min
	pos_nuInu_ankle = 0.7E-4*u.nW/(u.m**2*u.sr)
	pos_text_ankle = 1E-3*u.nW/(u.m**2*u.sr)	
	plot.annotate(ax, [posx_ankle], [pos_text_ankle], [text_ankle], pos_arrow = [[posx_ankle, pos_nuInu_ankle]], colors=[colors_UHECR[-1]], fontsize=11)

	#Annotation messengers
	x_spacing = 0.5
	labels_m = [r"$\gamma$", r"$\nu$ ", r"$p$ ", r"$^{A}_{Z}X$ "]
	list_posx_m = [x_spacing*10*u.MHz, x_spacing*30*u.TeV, x_spacing*0.2*u.EeV, x_spacing*0.2*u.EeV]
	list_posy_m = np.array([5E-5, 5E-5, 5E-5, 1E-3])*u.nW/(u.m**2*u.sr)
	plot.annotate(ax, list_posx_m, list_posy_m, labels_m, fontsize=24, ha='right')
	
	#Integrals
	list_posx = [0.3*u.GHz] + list_posx[1:-1] + [	1*u.EeV,#Hillas-C_p	
													40*u.PeV,#Hillas-B			
													18*u.EeV]#Hillas-C_nucl
									
	unit_text = u.eV/u.m**3#u.nW/(u.m**2*u.sr)
	list_units = [unit_text]*len(list_posx)
	for i in [-3,-2,-1]: list_units[i]=unit_text#Note: nucl above ankle: 12.0 +/- 3.0 meV / m3 in Auger PRL Features
	nuInu_min = plot.main_axes['y_axis']['val'][0]*plot.unit_nuInu
	list_pos_nuInu_u = [2*nuInu_min]*len(list_posx)
	nuInu_integrals = nuInu_integrals + nuInu_integrals_UHECR#concatenate
	def text_u(nuInu_int, unit_u = u.eV/u.m**3):		
		fact = 1
		if unit_u != u.nW/(u.m**2*u.sr): fact = (4*np.pi*u.sr/const.c)#to be changed with a try conversion -> ...
		u_list = [(nuInu*fact).to(unit_u) for nuInu in nuInu_int]
		return format_with_uncertainties(u_list)
	
	list_utext = [text_u(nuInu_int, list_units[i]) for i, nuInu_int in enumerate(nuInu_integrals)]
	plot.annotate(ax, list_posx, list_pos_nuInu_u, list_utext, rotation = +90, colors=colors + colors_UHECR)#facecolors=['w']*len(list_utext), edgecolors=colors + colors_UHECR)	

	#Save
	MySaveFig(fig, 'The_Fitted_MM_EGAL_Background_2023')

def The_ECRB(col, ms=10, is_direct=False, is_measurement=True, is_count=False, is_write_caption_data=True, rescale_E=False, show_protons=True, is_fit=True):
	#Principal unit system
	x_axis = dict(	unit = u.EeV, 
					val = [0.005,300],
					title= r'Energy, $E$ [EeV]')
	y_axis = dict(	unit = u.GeV/(u.m**2*u.s*u.sr),
					val = [0.5E-6, 2],
					title= r'$E^{2} {\rm d}J/{\rm d}E$ [GeV m$^{-2}$ s$^{-1}$ sr$^{-1}$]')
	principal_unit_system = dict(x_axis = x_axis, y_axis = y_axis)

	#Set the figure
	plot = TheSpectrum(main_axes = principal_unit_system, 
						ms = 12, lw = 3, label_type='label', show_gray_errors=True, is_write_caption_data=is_write_caption_data)
	fig, ax = plot.FigSetup(Shape='Rectangular')
	plot.ResetColorCycle(ax, col)
	
	#Plot and save
	plot.ecrb(ax, lower_legend=True, posx = 1.5*x_axis['val'][0]*x_axis['unit'], rescale_E=rescale_E, show_protons=show_protons)
	plot.experiment_legend(ax, delta_y=0.035)
	
	if is_fit:
		labels = ['ECRB']

		#All particle spectrum above 200 PeV
		nu_bounds = [0.2*u.EeV, 200*u.EeV]
		plot_allpart = TheSpectrum(main_axes = principal_unit_system, ms = 12, lw = 3, label_type='label', show_gray_errors=True, is_write_caption_data=is_write_caption_data)
		fig_allpart, ax_allpart = plot.FigSetup(Shape='Rectangular')
		plot_allpart.ResetColorCycle(ax_allpart, col)
		plot_allpart.ecrb(ax_allpart, lower_legend=True, posx = 1.5*x_axis['val'][0]*x_axis['unit'], rescale_E=True, show_protons=False, show_all_particles=True)
		plot_allpart.experiment_legend(ax_allpart, delta_y=0.035)			
		title = 'output_fit/The_AllParticleECRB_2023'
		if rescale_E: title+='rescaled_E'
		MySaveFig(fig_allpart, title)			

		#Define knots of the spline
		data_allpart = np.vstack(plot_allpart.dataset).T
		nu_min, nu_max = np.min(data_allpart[0]), np.max(data_allpart[0])
		delta_knots = 0.5
		lg_nu_min = np.log10(nu_min)-2*delta_knots
		lg_nu_max = np.log10(nu_max)+2*delta_knots		
		lg_nu_knots = list(np.arange(lg_nu_min, lg_nu_max, delta_knots))
	
		#Frame for the fitted dataset
		plot2_allpart = TheFit(data=data_allpart, 
						model_type="spline",
						init_with_model=None,
						lg_nu_knots = np.array(lg_nu_knots),
						main_axes = principal_unit_system, ms = 8, lw = 2)
		fig2_allpart, ax2_allpart = plot2_allpart.FigSetup(Shape='Rectangular')
		
		#Fit the dataset
		nsamples = 1E4
		save_flat_samples = f"output_fit/ecrb_allpart_samples"
		plot2_allpart.run_sampler(nsamples = int(nsamples), save_flat_samples=save_flat_samples, autocorr_time = 500, verbose=False)
		
		#Check the bounds and determines the integrals
		plot2_allpart.plot_components(nu_bounds, labels, save="ECRB_allpart")
		
		#Plot the dataset
		plot2_allpart.plot_dataset(ax2_allpart, color='k', is_plot_init_model=False)
		MySaveFig(fig2_allpart, 'output_fit/The_Fitted_ECRB_allpart_2023')	
		
		#Proton spectrum above 200 PeV
		nu_bounds = [0.2*u.EeV, 5*u.EeV]
		plot_protons = TheSpectrum(main_axes = principal_unit_system, ms = 12, lw = 3, label_type='label', show_gray_errors=True, is_write_caption_data=is_write_caption_data)
		fig_protons, ax_protons = plot.FigSetup(Shape='Rectangular')
		plot_protons.ResetColorCycle(ax_protons, col)
		plot_protons.ecrb(ax_protons, lower_legend=True, posx = 1.5*x_axis['val'][0]*x_axis['unit'], rescale_E=False, show_protons=True, show_all_particles=False)
		plot_protons.experiment_legend(ax_protons, delta_y=0.035)			
		title = 'output_fit/The_ProtonECRB_2023'
		if rescale_E: title+='rescaled_E'
		MySaveFig(fig_protons, title)			

		#Define knots of the spline
		data_protons = np.vstack(plot_protons.dataset).T
		nu_min, nu_max = np.min(data_protons[0]), np.max(data_protons[0])
		delta_knots = 0.5
		lg_nu_min = np.log10(nu_min)-2*delta_knots
		lg_nu_max = np.log10(nu_max)+2*delta_knots		
		lg_nu_knots = list(np.arange(lg_nu_min, lg_nu_max, delta_knots))
	
		#Frame for the fitted dataset
		plot2_protons = TheFit(data=data_protons, 
						model_type="spline",
						init_with_model=None,
						lg_nu_knots = np.array(lg_nu_knots),
						main_axes = principal_unit_system, ms = 8, lw = 2)
		fig2_protons, ax2_protons = plot2_protons.FigSetup(Shape='Rectangular')
		
		#Fit the dataset
		nsamples = 1E4
		save_flat_samples = f"output_fit/ecrb_protons_samples"
		plot2_protons.run_sampler(nsamples = int(nsamples), save_flat_samples=save_flat_samples, autocorr_time = 500, verbose=False)
		
		#Check the bounds and determines the integrals
		plot2_protons.plot_components(nu_bounds, labels, save="ECRB_protons")
		
		#Plot the dataset
		plot2_protons.plot_dataset(ax2_protons, color='k', is_plot_init_model=False)
		MySaveFig(fig2_protons, 'output_fit/The_Fitted_ECRB_protons_2023')	
	
	#Legend
	handles, labels = ax.get_legend_handles_labels()
	if is_direct: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='v', label='Dark patch')) 
	if is_measurement:
		if show_protons: 
			handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
							marker='o', label='Measurement: protons \& nuclei'))
			handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
							marker='s', label='Inference: protons only'))
		else: 
			handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
							marker='o', label='Measurement')) 
	if is_count: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='^', label='Galaxy count')) 	
	ax.legend(handles=handles, loc='upper right', labelspacing = 0.3)
	
	title = 'output_data/The_ECRB_2023'
	if rescale_E: title+='rescaled_E'
	MySaveFig(fig, title)
	
	
def The_ENB(col, ms=10, is_direct=False, is_measurement=True, is_count=False, is_write_caption_data=True, is_fit=True):
	#Principal unit system
	x_axis = dict(	unit = u.PeV, val = [0.02,5],
					title= r'Energy, $E$ [PeV]')
	y_axis = dict(	unit = u.GeV/(u.m**2*u.s*u.sr), val = [1E-5, 2E-3],
					title= r'$E^{2} {\rm d}J/{\rm d}E$ [GeV m$^{-2}$ s$^{-1}$ sr$^{-1}$]')
	principal_unit_system = dict(x_axis = x_axis, y_axis = y_axis)

	#Set the figure
	plot = TheSpectrum(main_axes = principal_unit_system, 
						ms = 12, lw = 3, label_type='label', show_gray_errors=True, is_write_caption_data=is_write_caption_data)
	fig, ax = plot.FigSetup(Shape='Rectangular')
	plot.ResetColorCycle(ax, col)
	
	#Plot and save
	plot.enb(ax, lower_legend=False, posx = 1.5*x_axis['val'][0]*x_axis['unit'])
	plot.experiment_legend(ax, delta_y=0.035)

	if is_fit:
		labels, nu_bounds = ['ENB'], [30*u.TeV, 3*u.PeV]
		plot2 = TheFit(data=np.vstack(plot.dataset).T, model_type="pwl",
						main_axes = principal_unit_system, ms = 8, lw = 2)
		fig2, ax2 = plot2.FigSetup(Shape='Rectangular')
		
		#Fit the dataset
		nsamples = 1E4
		save_flat_samples = f"output_fit/enb_samples"
		plot2.run_sampler(nsamples = int(nsamples), save_flat_samples=save_flat_samples, autocorr_time=50, verbose=False)
		
		#Check the bounds and determines the integrals
		plot2.plot_components(nu_bounds, labels, save="ENB")
		
		#Plot the dataset
		plot2.plot_dataset(ax2, color='k')
		MySaveFig(fig2, 'output_fit/The_Fitted_ENB_2023')
	
	#Legend
	handles, labels = ax.get_legend_handles_labels()
	if is_direct: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='v', label='Dark patch')) 
	if is_measurement: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='o', label='Measurement')) 
	if is_count: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='^', label='Galaxy count')) 	
	ax.legend(handles=handles, loc='upper right', labelspacing = 0.3)
		
	MySaveFig(fig, 'output_data/The_ENB_2023')
	
	
def The_CXB_CGB(colCXB, colCGB, ms=10,  is_direct=False, is_measurement=True, is_count=False, is_write_caption_data=False, is_fit=True):
	#Principal unit system
	x_axis = dict(	unit = u.MeV, val = [0.05E-3,3E6],
					title= r'Energy, $E$ [GeV]')
	y_axis = dict(	unit = u.GeV/(u.m**2*u.s*u.sr), val = [0.5E-4, 1],
					title= r'$E^{2} {\rm d}J/{\rm d}E$ [GeV m$^{-2}$ s$^{-1}$ sr$^{-1}$]')
	principal_unit_system = dict(x_axis = x_axis, y_axis = y_axis)

	#Set the figure
	plot = TheSpectrum(main_axes = principal_unit_system, 
						ms = 12, lw = 3, label_type='label', show_gray_errors=True, is_write_caption_data=is_write_caption_data)
	fig, ax = plot.FigSetup(Shape='Between')
	
	#Plot and save
	plot.ResetColorCycle(ax, colCXB)
	plot.cxb(ax, lower_legend=True, posx = 0.2*u.keV, include_Comptel=False)	
	plot.ResetColorCycle(ax, colCGB)	
	plot.cgb(ax, lower_legend=True, posx = 50*u.MeV)
	plot.model_all(ax)

	if is_fit:
		labels = ['CXB', 'CGB']
		nu_bounds = [300*u.eV, 2*u.MeV, 1*u.TeV]

		#Define knots of the spline
		data = np.vstack(plot.dataset).T
		nu_min, nu_max = np.min(data[0]), np.max(data[0])
		delta_knots = 0.5
		lg_nu_min = np.log10(nu_min)-1.*delta_knots
		lg_nu_max = np.log10(nu_max)+1.5*delta_knots		
		lg_nu_knots = list(np.arange(lg_nu_min, lg_nu_max, delta_knots))
	
		#Frame for the fitted dataset
		plot2 = TheFit(data=data, 
						model_type="spline",
						init_with_model="model/khaire2019.ecsv",
						lg_nu_knots = np.array(lg_nu_knots),
						main_axes = principal_unit_system, ms = 8, lw = 2)
		fig2, ax2 = plot2.FigSetup(Shape='Between')
		
		#Fit the dataset
		nsamples = 1E5
		save_flat_samples = f"output_fit/cxbcgb_samples"
		plot2.run_sampler(nsamples = int(nsamples), save_flat_samples=save_flat_samples)
		
		#Check the bounds and determines the integrals
		plot2.plot_components(nu_bounds, labels, save="CXB_CGB")
		
		#Plot the dataset
		plot2.plot_dataset(ax2, color='k', is_plot_init_model=False)
		MySaveFig(fig2, 'output_fit/The_Fitted_CXB_CGB_2023')

	plot.experiment_legend(ax, delta_y=0.03)	
	#Legend
	handles, labels = ax.get_legend_handles_labels()
	if is_direct: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='v', label='Dark patch')) 
	if is_measurement: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='o', label='Measurement')) 
	if is_count: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='^', label='Galaxy count')) 	
	ax.legend(handles=handles, loc='upper right', labelspacing = 0.3)
	
	MySaveFig(fig, 'output_data/The_CXB_CGB_2023')	

	
def The_CGB(col, ms=10,  is_direct=False, is_measurement=True, is_count=False, is_write_caption_data=True):
	#Principal unit system
	x_axis = dict(	unit = u.GeV, val = [0.004,1E3],
					title= r'Energy, $E$ [GeV]')
	y_axis = dict(	unit = u.GeV/(u.m**2*u.s*u.sr), val = [1E-4, 1E-1],
					title= r'$E^{2} {\rm d}J/{\rm d}E$ [GeV m$^{-2}$ s$^{-1}$ sr$^{-1}$]')
	principal_unit_system = dict(x_axis = x_axis, y_axis = y_axis)

	#Set the figure
	plot = TheSpectrum(main_axes = principal_unit_system, 
						ms = 12, lw = 3, label_type='label', show_gray_errors=True, is_write_caption_data=is_write_caption_data)
	fig, ax = plot.FigSetup(Shape='Rectangular')
	plot.ResetColorCycle(ax, col)
	
	#Plot and save
	plot.cgb(ax, lower_legend=True, posx = 1.5*x_axis['val'][0]*x_axis['unit'])
	plot.experiment_legend(ax, delta_y=0.035)
	plot.model_all(ax)
	plot.model_cgb(ax)
	
	#Legend
	handles, labels = ax.get_legend_handles_labels()
	if is_direct: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='v', label='Dark patch')) 
	if is_measurement: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='o', label='Measurement')) 
	if is_count: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='^', label='Galaxy count')) 	
	ax.legend(handles=handles, loc='upper right', labelspacing = 0.3)
	
	MySaveFig(fig, 'output_data/The_CGB_2023')
	
	
def The_CXB(col, ms=10,  is_direct=True, is_measurement=True, is_count=False, is_write_caption_data=True):
	#Principal unit system
	x_axis = dict(	unit = u.keV, val = [0.08,4E3],
					title= r'Energy, $E$ [keV]')
	y_axis = dict(	unit = u.GeV/(u.m**2*u.s*u.sr), val = [0.5E-2, 2E0],
					title= r'$E^{2} {\rm d}J/{\rm d}E$ [GeV m$^{-2}$ s$^{-1}$ sr$^{-1}$]')
	principal_unit_system = dict(x_axis = x_axis, y_axis = y_axis)

	#Set the figure
	plot = TheSpectrum(main_axes = principal_unit_system, 
						ms = 12, lw = 3, label_type='label', show_gray_errors=True, is_write_caption_data=is_write_caption_data)
	fig, ax = plot.FigSetup(Shape='Rectangular')
	plot.ResetColorCycle(ax, col)
	
	#Plot and save
	plot.cxb(ax, lower_legend=True, posx = 1*u.keV, include_Comptel=True)
	plot.experiment_legend(ax, delta_y=0.035)
	plot.model_all(ax)
	plot.model_cxb(ax)

	#legend
	handles, labels = ax.get_legend_handles_labels()
	if is_direct: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='v', label='Dark patch')) 
	if is_measurement: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='o', label='Measurement')) 
	if is_count: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='^', label='Galaxy count')) 	
	ax.legend(handles=handles, loc='upper left', labelspacing = 0.3)

	MySaveFig(fig, 'output_data/The_CXB_2023')


def The_EBL(colCIB, colCOB, ms=10,  is_direct=True, is_measurement=True, is_count=True, is_write_caption_data=True, is_primary_wavelength=True, annotate=True, is_fit=True):
	#Principal unit system
	if is_primary_wavelength:
		val=[5E-2,2E3]
		x_axis = dict(	unit = u.um, 
						val = val,
						title= r'Wavelength, $\lambda$ [µm]')
	else:
		val=[1E11,1E16]
		x_axis = dict(	unit = u.Hz, 
						val = val,
						title= r'Frequency, $\nu$ [Hz]')

	val=[3E-2, 1E2]
	y_axis = dict(	unit = u.nW/(u.m**2*u.sr), 
					val = val,
					title= r'$\nu I_\nu$ [nW m$^{-2}$ sr$^{-1}$]')
	principal_unit_system = dict(x_axis = x_axis, y_axis = y_axis)

	#Secondary unit system
	if is_primary_wavelength:
		x_axis2 = dict(	unit = u.Hz, 
						title= r'Frequency, $\nu$ [Hz]')
	else:
		x_axis2 = dict(	unit = u.um, 
						title= r'Wavelength, $\lambda$ [µm]')
	
	secondary_unit_system = dict(x_axis = x_axis2, y_axis = None)
	
	#Set the figure
	plot = TheSpectrum(main_axes = principal_unit_system, sec_axes = secondary_unit_system, 
						ms = 12, lw = 3, label_type='label', show_gray_errors=True, is_write_caption_data=is_write_caption_data)
	fig, ax = plot.FigSetup(Shape='Between')
	
	#Plot and save
	if is_primary_wavelength:
		plot.ResetColorCycle(ax, colCIB)
		plot.cib(ax, lower_legend=True, posx = 20*u.um)
		plot.ResetColorCycle(ax, colCOB)
		plot.clean_ref_id()
		plot.cob(ax, lower_legend=True, posx = 0.2*u.um)	
	else:
		plot.ResetColorCycle(ax, colCIB)
		plot.cib(ax, lower_legend=True, posx = 10*x_axis['val'][0]*x_axis['unit'])
		plot.ResetColorCycle(ax, colCOB)
		plot.clean_ref_id()
		plot.cob(ax, lower_legend=True, posx = 1E3*x_axis['val'][0]*x_axis['unit'])	

	plot.model_all(ax)
	
	if is_fit:
		labels = ['COB','CIB']
		nu_bounds = [100*u.nm, 10*u.um, 2*u.mm]

		#Define knots of the spline
		data = np.vstack(plot.dataset).T
		nu_min, nu_max = np.min(data[0]), np.max(data[0])
		delta_knots = 0.5
		lg_nu_min = np.log10(nu_min)-1*delta_knots
		lg_nu_max = np.log10(nu_max)+1.5*delta_knots		
		lg_nu_knots = list(np.arange(lg_nu_min, lg_nu_max, delta_knots))
	
		#Frame for the fitted dataset
		plot2 = TheFit(data=data, 
						model_type="spline",
						init_with_model="model/khaire2019.ecsv",
						lg_nu_knots = np.array(lg_nu_knots),
						main_axes = principal_unit_system, 
						sec_axes = secondary_unit_system, ms = 8, lw = 2)
		fig2, ax2 = plot2.FigSetup(Shape='Between')
		
		#Fit the dataset
		nsamples = 1E5
		save_flat_samples = f"output_fit/ebl_samples"
		plot2.run_sampler(nsamples = int(nsamples), save_flat_samples=save_flat_samples)
		
		#Check the bounds and determines the integrals
		plot2.plot_components(nu_bounds, labels, save="EBL")
		
		#Plot the dataset
		plot2.plot_dataset(ax2, color='k', is_plot_init_model=False)
		MySaveFig(fig2, 'output_fit/The_Fitted_EBL_2023')
			
	
	#Annotations
	if annotate:
		labels = [r'COB', r'CIB']
		list_posx = [1*u.um, 150*u.um]
		list_pos_nuInu = [1.5*u.nW/(u.m**2*u.sr)]*len(list_posx)
		plot.annotate(ax, list_posx, list_pos_nuInu, labels, ha='center')
		
	#legend
	handles, labels = ax.get_legend_handles_labels()

	plot.experiment_legend(ax, delta_y=0.027, reverse = is_primary_wavelength)
	if is_direct: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
									marker='v', label='Dark patch')) 
	if is_measurement: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
									marker='o', label='Measurement')) 
	if is_count: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
									marker='^', label='Galaxy count')) 
	ax.legend(handles=handles, loc='upper left', labelspacing = 0.3)
	
	fileout = 'The_EBL_2023'
	MySaveFig(fig, 'output_data/'+fileout)


def The_EBL_with_models(colCIB, colCOB, ms=10,  is_direct=True, is_measurement=True, is_count=True, is_write_caption_data=False, is_primary_wavelength=True, imodels=0):

	#Principal unit system
	if is_primary_wavelength:
		val=[5E-2,2E3]
		x_axis = dict(	unit = u.um, val = val,
						title= r'Wavelength, $\lambda$ [µm]')
	else:
		val=[5E11,5E15]
		x_axis = dict(	unit = u.Hz, val = val,
						title= r'Frequency, $\nu$ [Hz]')

	val=[1E-1, 0.3E3]
	y_axis = dict(	unit = u.nW/(u.m**2*u.sr), 
					val = val,
					title= r'$\nu I_\nu$ [nW m$^{-2}$ sr$^{-1}$]')
	principal_unit_system = dict(x_axis = x_axis, y_axis = y_axis)

	#Secondary unit system
	if is_primary_wavelength:
		x_axis2 = dict(	unit = u.Hz, 
						title= r'Frequency, $\nu$ [Hz]')
	else:
		x_axis2 = dict(	unit = u.um, 
						title= r'Wavelength, $\lambda$ [µm]')
	
	secondary_unit_system = dict(x_axis = x_axis2, y_axis = None)
	
	#Set the figure
	show_gray_errors = (imodels==None)
	plot = TheSpectrum(main_axes = principal_unit_system, sec_axes = secondary_unit_system, 
						ms = ms, lw = 3, label_type='observatory', show_gray_errors=show_gray_errors, is_write_caption_data=is_write_caption_data)
	fig, ax = plot.FigSetup(Shape='Rectangular')
	
	#Plot and save
	if is_primary_wavelength:
		plot.ResetColorCycle(ax, colCIB)
		plot.cib(ax, lower_legend=True, posx = 40*u.um)
		plot.ResetColorCycle(ax, colCOB)		
		plot.cob(ax, lower_legend=True, posx = 0.4*u.um)	
	else:
		plot.ResetColorCycle(ax, colCIB)
		plot.cib(ax, lower_legend=True, posx = 10*x_axis['val'][0]*x_axis['unit'])
		plot.ResetColorCycle(ax, colCOB)
		plot.cob(ax, lower_legend=True, posx = 1E3*x_axis['val'][0]*x_axis['unit'])	

	if imodels==None: plot.model_all(ax)
	else: 
		plot.ResetColorCycle(ax)
		plot.model_cob(ax, imodels=imodels)

	#legend
	loc='upper left'
	ax.legend(loc=loc, labelspacing = 0.1)
	
	fileout = 'The_EBL_with_models_2023'
	if imodels!=None: fileout+=f'_models{imodels}'
	MySaveFig(fig, 'output_data/'+fileout)
	

def The_ZL(ms=10,  is_direct=True, is_measurement=True, is_count=False, is_write_caption_data=True):
	#Principal unit system
	x_axis = dict(	unit = u.um, val = [1E-1,6],
					title= r'Wavelength, $\lambda$ [µm]')

	y_axis = dict(	unit = u.nW/(u.m**2*u.sr), val = [0.3, 5E2],
					title= r'$\nu I_\nu$ [nW m$^{-2}$ sr$^{-1}$]')
	principal_unit_system = dict(x_axis = x_axis, y_axis = y_axis)


	#Set the figure
	plot = TheSpectrum(main_axes = principal_unit_system,
						ms = 12, lw = 3, label_type='label', show_gray_errors=True, is_write_caption_data=is_write_caption_data)
	fig, ax = plot.FigSetup(Shape='Rectangular')

	#Plot ZL in color
	plot.ResetColorCycle(ax)
	plot.zodi(ax, lower_legend=True, posx = 0.6*u.um)

	plot.experiment_legend(ax, delta_y = 0.035)
	plot.model_all(ax)
	plot.model_zl(ax, nuInu_550nm=1*132*u.nW/(u.sr*u.m**2))

	#Legend
	handles, labels = ax.get_legend_handles_labels()
	if is_direct: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='v', label='Dark patch')) 
	if is_measurement: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='o', label='Measurement')) 
	if is_count: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='^', label='Galaxy count')) 	
	ax.legend(handles=handles, loc='upper left', labelspacing = 0.3)
	
	MySaveFig(fig, 'output_data/The_ZL_2023')
		
		
def The_CRB(col, ms=10,  is_direct=True, is_measurement=False, is_count=True, is_write_caption_data=True, is_fit=True):
	#Principal unit system
	x_axis = dict(	unit = u.GHz, val = [1E-2,100],
					title= r'Frequency, $\nu$ [GHz]')
	y_axis = dict(	unit = u.nW/(u.m**2*u.sr), val = [0.3E-6, 3E-1],
					title= r'$\nu I_\nu$ [nW m$^{-2}$ sr$^{-1}$]')
	principal_unit_system = dict(x_axis = x_axis, y_axis = y_axis)

	#Set the figure
	plot = TheSpectrum(main_axes = principal_unit_system, 
						ms = 12, lw = 3, label_type='label', show_gray_errors=True, is_write_caption_data=is_write_caption_data)
	fig, ax = plot.FigSetup(Shape='Rectangular')
	plot.ResetColorCycle(ax, col)
	
	#Plot
	plot.crb(ax, lower_legend=True, posx = 1.5*x_axis['val'][0]*x_axis['unit'])
	plot.experiment_legend(ax, delta_y=0.04)
	plot.model_all(ax)
	plot.model_crb(ax)
	
	#Fit
	if is_fit:
		labels, nu_bounds = ['CRB'], [10*u.MHz, 10*u.GHz]
		plot2 = TheFit(data=np.vstack(plot.dataset).T, model_type="ssa_pwl",
						main_axes = principal_unit_system, ms = 8, lw = 2)
		fig2, ax2 = plot2.FigSetup(Shape='Rectangular')
		
		#Fit the dataset
		nsamples = 1E4
		save_flat_samples = f"output_fit/crb_samples"
		plot2.run_sampler(nsamples = int(nsamples), save_flat_samples=save_flat_samples, autocorr_time=50, verbose=False)
		
		#Check the bounds and determines the integrals
		plot2.plot_components(nu_bounds, labels, save="CRB")
		
		#Plot the dataset
		plot2.plot_dataset(ax2, color='k', is_plot_init_model=False)
		MySaveFig(fig2, 'output_fit/The_Fitted_CRB_2023')	

	#Legend
	handles, labels = ax.get_legend_handles_labels()
	if is_direct: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='v', label='Dark patch')) 
	if is_measurement: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='o', label='Measurement')) 
	if is_count: handles.append(mlines.Line2D([], [], color='gray', linestyle='None', ms=ms, fillstyle='none', 
								marker='^', label='Galaxy count')) 	
	ax.legend(handles=handles, loc='upper left', labelspacing = 0.25)
	
	MySaveFig(fig, 'output_data/The_CRB_2023')
	
	
if __name__ == "__main__":

	#All MM data
	colECRB, colENB, colCGB, colCXB, colCOB, colCIB, colCRB = The_MM_EGAL_Background()

	#Radio background data and fit
	The_CRB(colCRB)

	#Optical/infrared background data, models, contaminants and fit
	The_EBL(colCIB, colCOB)	
	The_EBL_with_models(colCIB, colCOB)
	The_ZL()
		
	#X-ray, gamma-ray background data and fit
	The_CXB(colCXB)	
	The_CGB(colCGB)
	The_CXB_CGB(colCXB, colCGB)	

	#Neutrino background and fit
	The_ENB(colENB)

	#Cosmic-ray background and fit
	The_ECRB(colECRB, rescale_E=False)

	#Fitted backgrounds	- load samples generated by methods above
	The_Fitted_MM_EGAL_Background()	
